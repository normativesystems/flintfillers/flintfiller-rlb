import pandas as pd
import os

from action_filter.filter_action_sentences import read_action_words
from action_filter.filter_action_sentences import is_unsolved_reference
from action_filter.filter_action_sentences import check_if_definition
from action_filter.filter_action_sentences import check_action_verb_root_and_copula


def test_is_unsolved_reference():
    d = {'Brontekst': ["In afwijking van de  en  is  ", "Een aanwijzing op grond van  ,   ,  of op grond van  , "
        , "Besluiten die zijn genomen op grond van de  ,   ,   ,   ,   ,   ,   ,   ,  ",
                       "Ik ga op de, fiets naar mijn werk"]}
    test_df = pd.DataFrame(data=d)

    assert is_unsolved_reference(test_df.iloc[0]), "Should be true, multiple spaces after 'de' found"
    assert is_unsolved_reference(test_df.iloc[1]), "Should be true, multiple comma's in succession"
    assert is_unsolved_reference(test_df.iloc[2]), "Should be true, multiple comma's in succession"

    assert not is_unsolved_reference(test_df.iloc[3]), "Should be false, no unsolved references"


def test_check_if_definition():
    d = {'Brontekst': ["This sentence endswith ;", "This sentence does not", "This sentence ; is also a definition",
                       "This is a correct sentence"],
         'opsomming': ["Artikel1.6", "Artikel11", "Artikel16", "Artikel1a:1"]}

    test_df = pd.DataFrame(data=d)

    assert check_if_definition(test_df.iloc[0]), "Should be true, sentence endswith ; and has a variant of 'Artikel1'"
    assert not check_if_definition(test_df.iloc[1]), "Should be false, sentence does not end with ; or : and does " \
                                                     "not have a Artikel1 variant"
    assert check_if_definition(test_df.iloc[2]), "Should be true, a ; is found in the first three tokens"
    assert check_if_definition(test_df.iloc[3]), "Should be true, the sentence is correct but the artikel variant not"


def test_check_action_verb_root():
    cwd = os.getcwd()
    action_sentence_list = read_action_words(os.path.join(cwd, "test_data", "action_sentences.xlsx"))

    test_sent1 = pd.Series({'Brontekst': "Om gewichtige redenen kan het onderzoek ter zitting geheel of gedeeltelijk "
                                         "plaatsvinden met gesloten deuren ."})

    test_sent2 = pd.Series(
        {"Brontekst": "Op straffe van nietigheid worden de beschikkingen ,  vonnissen en arresten in "
                      "burgerlijke zaken en strafzaken gewezen en de uitspraken in bestuursrechtelijke zaken "
                      "gedaan met het in deze wet bepaaldeaantal rechterlijke ambtenaren met rechtspraak "
                      "belast ."})

    test_sent3 = pd.Series(
        {"Brontekst": "Indien bij de wet is bepaald dat ook anderen dan rechterlijke ambtenaren deel uitmaken "
                      "van een meervoudige kamer ,  zijn de beslissingen van de desbetreffende meervoudige "
                      "kamer tevens nietig ,  indien deze beslissingen niet zijn genomen met het in deze "
                      "wet bepaalde aantal personen ,  niet zijnde rechterlijk ambtenaar ."})

    test_sent4 = pd.Series(
        {"Brontekst": "Het Rijk ,  de provincie ,  de gemeente en het waterschap is verplicht een weg te "
                      "onderhouden ,  wanneer dat openbare lichaam dien tot openbaren weg heeft bestemd . "})

    assert not check_action_verb_root_and_copula(pd.Series(test_sent1), action_sentence_list)[0], \
        "Should be false, ROOT is 'plaatsvinden', which is not  in action_verb_list"
    assert not check_action_verb_root_and_copula(pd.Series(test_sent1), action_sentence_list)[1], \
        "Should be false, there is no copula before the root"

    assert check_action_verb_root_and_copula(pd.Series(test_sent2), action_sentence_list)[0], \
        "Should be true, ROOT is 'wijzen', which is in action_verb_list"
    assert not check_action_verb_root_and_copula(pd.Series(test_sent2), action_sentence_list)[1], \
        "Should be false, there is no copula before the root"

    assert check_action_verb_root_and_copula(pd.Series(test_sent3), action_sentence_list)[0], \
        "Should be true, ROOT is 'bepaald' which is in action_verb_list"
    assert not check_action_verb_root_and_copula(pd.Series(test_sent3), action_sentence_list)[1], \
        "Should be false, there is no copula before the root"

    assert check_action_verb_root_and_copula(pd.Series(test_sent4), action_sentence_list)[0], \
        "Should be true, ROOT is 'verplichten' which is in the action verb list"
    assert check_action_verb_root_and_copula(pd.Series(test_sent4), action_sentence_list)[1], \
        "Should be true, there is a copula before the root"
