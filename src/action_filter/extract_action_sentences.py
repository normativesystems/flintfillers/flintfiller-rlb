"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import argparse
import os
import math
import re
import pandas as pd
import string
import sys

from ast import literal_eval
from typing import List
from nltk import tokenize
from collections import defaultdict

from flintfiller import chunk_tag_dataframe
from juridecompose import wetten_xml_to_dict
from juridecompose import dict_to_dataframe

cwd = os.getcwd()
sys.path.append(os.path.join(cwd, "src"))
sys.path.append(os.path.join(cwd, "src", "juridecompose"))


def list_files(directory):
    file_list = []

    for root, dirs, files in os.walk(os.path.abspath(directory)):
        for name in files:
            file_list.append(os.path.join(root, name))
    return file_list


def space_punctuations(text):
    for punctuation in string.punctuation:
        text = text.replace(punctuation, ' ' + punctuation + ' ')
    return text


def split_sentences(list_of_dataframes: List[pd.DataFrame]):
    """
    :param list_of_dataframes:
    :return: a list_of_dataframes where the brontekst is split on a new line when one row of brontekst
    spans more than one sentence. Sentences are split using sentence tokenizer from NLTK.
    """
    store_dataframes = []
    for dataframe in list_of_dataframes:
        all_sentences_title = []
        for tekst, title, opsomming, opsomming2 in zip(list(dataframe['Brontekst']), list(dataframe['law_name'])
                , list(dataframe['Opsomming niveau 1']), list(dataframe['Opsomming niveau 2'])):
            tekst_split = tokenize.sent_tokenize(tekst, language="dutch")
            if len(tekst_split) == 1:
                all_sentences_title.append((space_punctuations(tekst_split[0]), title, opsomming, opsomming2))
            else:
                for sent in tekst_split:
                    all_sentences_title.append((space_punctuations(sent), title, opsomming, opsomming2))

        all_sentences_title_as_dataframe = pd.DataFrame(all_sentences_title, columns=["Brontekst", "law_name"
                                                                                      , "opsomming", "opsomming2"])
        store_dataframes.append(all_sentences_title_as_dataframe)
    return store_dataframes


def xml_to_dataframe_eas() -> List[pd.DataFrame]:
    """
    Reads a single .xml or a list of .xml files and transforms these files in a dataframe.
    Extracts the title based on a regex pattern.
    :return: A list of dataframes for each .xml that is succesfully extracted. Also stores a .csv file that contains
    the errors of laws that could not be parsed.
    """
    args = parse_commandline_arguments()
    list_of_law_dataframes = []

    list_of_errors = []
    if args.xml and args.dict_file:
        xml_file = args.xml
        output_dict = args.dict_file
        wetten_dict = wetten_xml_to_dict.parse_xml_artikelen(xml_file, output_dict)

        if args.df_file:
            output_df = args.df_file
            try:
                xml_df = dict_to_dataframe.dict_to_dataframe(wetten_dict, output_df)
                # extract the name of the law, assume following format,
                # regex pattern checks between: _digit_ and .xml in files named like:
                # "data/xml_files/BWBR0001840_2018-12_2_Grondwet.xml"
                # TO DO: make regex pattern extraction more loose
                regex_pattern = r"(?<=_\d_)(.*)(?=.xml)"
                law_name = re.search(regex_pattern, xml_file)
                xml_df['law_name'] = str(law_name[0])
                list_of_law_dataframes.append(xml_df)
            except ValueError as ve:
                list_of_errors.append(("ValueError", ve, str(law_name[0])))
            except KeyError as ke:
                list_of_errors.append(("KeyError", ke, str(law_name[0])))
            except TypeError as te:
                list_of_errors.append(("TypeError", te, str(law_name[0])))

        else:
            print(f'finished with parsing xml to dictionary, check file: ' + output_dict)

    elif args.law_folder:
        law_folder_path = args.law_folder
        list_of_laws = list_files(law_folder_path)
        for law in list_of_laws:
            if law.endswith(".xml"):
                output_dict = args.dict_file
                wetten_dict = wetten_xml_to_dict.parse_xml_artikelen(law, output_dict)
                if args.df_file:
                    output_df = args.df_file
                    # extract the name of the law, assume following format,
                    # regex pattern checks between: _digit_ and .xml in files named like:
                    # "data/xml_files/BWBR0001840_2018-12_2_Grondwet.xml"
                    regex_pattern = r"(?<=_\d_)(.*)(?=.xml)"
                    law_name = re.search(regex_pattern, law)
                    try:
                        xml_df = dict_to_dataframe.dict_to_dataframe(wetten_dict, output_df)
                        xml_df['law_name'] = str(law_name[0])
                        list_of_law_dataframes.append(xml_df)
                    except ValueError as ve:
                        list_of_errors.append(("ValueError", ve, str(law_name[0])))
                    except KeyError as ke:
                        list_of_errors.append(("KeyError", ke, str(law_name[0])))
                    except TypeError as te:
                        list_of_errors.append(("TypeError", te, str(law_name[0])))

            # law ends with .json
            # TO DO: fix type error: TypeError: string indices must be integers // ignore for now
            else:
                pass
        df = pd.DataFrame(list_of_errors, columns=['law', 'error_name', 'error'])
        df.to_csv(os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2022", "errors_parsing_laws.csv"), sep=';')
    else:
        print('you did not pass the right combination of arguments, parsing not possible.')
    # split sentences using NLTK sentence tokenizer and drop all columns except for ['Brontekst'] and ['law_name']
    list_of_law_dataframes = split_sentences(list_of_law_dataframes)
    # remove placeholder file
    os.remove(output_dict)

    return list_of_law_dataframes


def chunk_list_of_dataframes(list_of_df: List[pd.DataFrame]) -> List[pd.DataFrame]:
    """
    Use the function to chunk a dataframe which is defined in flintfiller.chunk_tag_dataframe. In this function a file
    is created, here we use a placeholder location and remove the placeholder file later on.
    :param list_of_df: list of dataframes
    :return: updated list of dataframes containing columns for part of speech tags and verbs
    """
    store_new_df = []
    # "src/resource/data/annotations/placeholder.csv"
    placeholder_filepath = os.path.join(cwd, "src", "resource", "data", "annotations", "placeholder.csv")
    for law_df in list_of_df:
        chunked_tag_df = chunk_tag_dataframe.parser("pattern", law_df, placeholder_filepath)
        os.remove(placeholder_filepath)

        store_new_df.append(chunked_tag_df)
    return store_new_df


def check_if_definition(row: pd.Series) -> bool:
    """
    Evaluates a sentence threefold 1) look for rows that end with either ";" or ":", 2) look for artikelnumbers such as
    ["Artikel1a:1", "Artikel1.2", "Artikel1:5", "Artikel 1"], but not: "Artikel11" and 3) look for
     sentences that have ";" or ":" at the beginning.
    :param row: iterable row that is created by dataframe.iterows(), this function uses row["opsomming"] and
    row["Brontekst"]
    :return: a boolean that is true when the input sentence is a definition because one of the evaluations is true,
    when false the sentence is not a definition.
    """
    if str(row["Brontekst"]).rstrip().endswith(';') or str(row["Brontekst"]).rstrip().endswith(':'):
        ends_with_colon = True
    else:
        ends_with_colon = False

    match = re.match("Artikel1+\D", str(row["opsomming"]))
    if bool(match) or str(row["opsomming"]) == "Artikel1":
        in_first_article = True
    else:
        in_first_article = False

    tokens = str(row["Brontekst"]).split(" ")
    if (";" or ":") in tokens[0:3]:
        colon_first_tokens = True
    else:
        colon_first_tokens = False

    # only if all values are False, it is not a definition
    bool_list = [ends_with_colon, in_first_article, colon_first_tokens]

    if any(bool_list):
        sentence_is_definition = True
    else:
        sentence_is_definition = False
    return sentence_is_definition


def filter_action_sentences(dataframe: pd.DataFrame, min_token_length: int = 7) -> pd.DataFrame:
    """
    Only select action sentences that start with a capital letter and are a not a definition (see check_if_definition).
    :param min_token_length: specifies the least amount of tokens an action sentence should have.
    :param dataframe:
    :return: a dataframe with sentences that adhere to the specified constraints
    """
    selected_rows = []
    for index, row in dataframe.iterrows():
        tokens = row["Brontekst"].split(" ")

        # only use a "Sentence has to start with capital" constraint for now
        # if len(tokens) > min_token_length and tokens[0].istitle():
        if tokens[0].istitle() and not check_if_definition(row):
            selected_rows.append(row)
    df = pd.DataFrame(selected_rows)
    return df


def read_action_words(filepath):
    """
    Reads and selects the action verbs that are labelled as action by selecting only the rows that have "ja" in the
    "action" column.
    :param filepath: location of the excel sheet.
    :return: list of the base of the action verbs.
    """
    action_words_excel = pd.read_excel(filepath, sheet_name="action_sentences_robert_oov3", engine='openpyxl')
    action_words_excel = action_words_excel[action_words_excel["action"] == "ja"]
    action_words = list(action_words_excel['base'])
    return action_words


def check_infinitive(inf, row, list_of_action_verbs) -> bool:
    return inf in list_of_action_verbs and not (
            ("het " + inf) in str(row['Brontekst']) or
            ("de " + inf) in str(row['Brontekst']) or
            ("een " + inf) in str(row['Brontekst']))


def add_sentence_id(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    Add a sentence_id column to the dataframe. The sentence ID is created by concatening the row index to the name of
    the law, i.e. "1-grondwet" specifies that the sampled row is the first sentence from the grondwet.
    """
    idx = list(dataframe.index)
    law_name = dataframe['law_name'].iloc[0]
    name_index = [str(x) + "-" + law_name for x in idx]
    dataframe['sentence_ID'] = name_index
    return dataframe


def store_dataframe(dataframe: pd.DataFrame) -> str:
    output_folder = os.path.join(cwd, "src", "resource", "data", "annotations", "all_parsed_laws_as_csv")
    file_name = dataframe['law_name'].iloc[0] + ".csv"
    full_path = os.path.join(output_folder, file_name)
    return dataframe.to_csv(full_path, index=False, sep=";")


def add_infinitve_original_verb(dataframe: pd.DataFrame, retrieved_action_infinitives: defaultdict,
                                retrieved_action_original) -> pd.DataFrame:
    dataframe['infinitive_verb'] = ""
    dataframe['original_verb'] = ""
    for inf_index, inf_verb in retrieved_action_infinitives.items():
        dataframe.at[inf_index, "infinitive_verb"] = inf_verb
    for original_index, original_verb in retrieved_action_original.items():
        dataframe.at[original_index, "original_verb"] = original_verb
    return dataframe


def retrieve_action_sentences(dataframe: pd.DataFrame, action_verb_path: str, write_to_disk: bool = True) -> pd.DataFrame:
    """
    Loop through the row that contains verbs in a sentence, check for each verb whether or not it is regarded as an
    action verb. This results in a list with either "True" or "False" values, in case one or more "True" values are
    found, the row is labelled as an action sentence using the presence_of_action_verb column.

    Also calls add_sentence_id and store_dataframe inside this function.
    :param write_to_disk: boolean value that specifies whether or not the dataframe should be saved to disk
    :param action_verb_path: path to the file that contains the list of action verbs
    :return: dataframe with an additional column that indicates if a verb is an action verb
    """
    action_verbs = read_action_words(action_verb_path)
    retrieved_action_infinitives = defaultdict(list)
    retrieved_action_original = defaultdict(list)
    check_action_word = []
    for index, row in dataframe.iterrows():
        verb_dict = literal_eval(str(row['verbs']))
        # check if one of the verbs is considered an action word
        store_tf = []
        for infinitive, original in verb_dict.items():
            if check_infinitive(infinitive, row, action_verbs) and not original == 'indien':
                store_tf.append(True)
                retrieved_action_infinitives[index].append(infinitive)
                retrieved_action_original[index].append(original)
            else:
                store_tf.append(False)
        # as soon as one action verb is found we want to store the sentence
        if True in store_tf:
            check_action_word.append("T")
        else:
            check_action_word.append("F")
    # append the infinitives and verbs to the dataframe as a sanity check
    dataframe = add_infinitve_original_verb(dataframe, retrieved_action_infinitives, retrieved_action_original)
    # append the list to the dataframe
    dataframe['presence_of_action_verb'] = check_action_word
    dataframe = add_sentence_id(dataframe)
    if write_to_disk:
        store_dataframe(dataframe)
    return dataframe


def sample_action_sentence(list_of_dataframes: List[pd.DataFrame], n_sentences: int = 50) -> pd.DataFrame:
    """
    Randomly sample n amount of sentences.
    :param list_of_dataframes:
    :param n_sentences: specifies the amount of sentences to be sampled in total, selecting a very high value will
    result in the selection of every possible sentence
    :return:
    """
    # determine the number of sentences extracted from each law
    required_sentences = math.ceil(n_sentences / len(list_of_dataframes))
    print("required sentences:", required_sentences)
    store_frames = []
    for df in list_of_dataframes:
        # store every action sentence
        # store_frames.append(df)
        # sample rows from each dataframe, sometimes there are not enough action sentences to sample
        # in that case, sample the entirety of the dataframe which are all available action sentences
        try:
            sample = df.sample(n=required_sentences, random_state=41)
            store_frames.append(sample)
        except ValueError:
            store_frames.append(df)

    # merge the small samples from each law
    merged_dataframes = pd.concat(store_frames, ignore_index=True)

    # extract destined file location and store the .csv file here
    args = parse_commandline_arguments()
    output_df = args.df_file

    # add user_ID column
    merged_dataframes['annotator_ID'] = ""
    merged_dataframes['annotator_ID'] = merged_dataframes['annotator_ID'].astype('object')
    for i in range(len(merged_dataframes)):
        merged_dataframes.at[i, 'annotator_ID'] = ["RvD", "MdB", "RB", "DV"]

    print("size zonder drop duplicates:", merged_dataframes.shape)
    merged_dataframes = merged_dataframes.drop_duplicates(subset=['Brontekst'])
    print("size als duplicates gedropped zijn:", merged_dataframes.shape)

    merged_dataframes = merged_dataframes.drop(['law_name', 'tags', 'verbs', 'presence_of_action_verb'], axis=1)
    merged_dataframes.to_csv(output_df, index=False, sep=";")
    print("outputfile stored in folder: {}".format(output_df))
    return merged_dataframes


def prepare_2022_sentences():
    # list of data frame(s)
    law_data_frames = xml_to_dataframe_eas()
    # add pos tags and infinitives to the dataframes
    law_data_frames_chunk_pos = chunk_list_of_dataframes(law_data_frames)
    # search the action sentences in each law
    action_sentences_2022 = []
    for law_df in law_data_frames_chunk_pos:
        action_sentence_df = retrieve_action_sentences(law_df, os.path.join(cwd, "src", "resource", "data",
                                                                            "annotations", "annotations_2022",
                                                                            "action_sentences.xlsx"))
        action_sentence_df = action_sentence_df.loc[(action_sentence_df["presence_of_action_verb"] == "T")]
        action_sentence_df['received_annotations'] = 0
        #action_sentence_df = filter_action_sentences(action_sentence_df)
        action_sentences_2022.append(action_sentence_df)

    # sample
    final_2022_df = sample_action_sentence(action_sentences_2022, 50000)
    final_2022_df = final_2022_df.drop(["opsomming2", "infinitive_verb", "original_verb", "annotator_ID"], axis=1)
    return final_2022_df


# i.e. python src/action_filter/main.py -lf src/resource/data/annotations/all_input_laws_as_xml -d src/resource/data/annotations/annotations_2022/placeholder.json -df src/resource/data/annotations/annotations_2022/test_action_filter_main.csv -fo src/resource/data/annotations/annotations_2022/final_output.csv
def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--xml',
                        help="input xml file")
    parser.add_argument('-d', '--dict_file',
                        help="location of file with dictionary of xml")
    parser.add_argument('-df', '--df_file',
                        help="outputfile of 2022 annotations")
    parser.add_argument('-fo', '--final_output_path',
                        help="output path of the complete file that is filtered and has annotators assigned")
    parser.add_argument('-lf', '--law_folder',
                        help="location of law folder containing laws as xml or json.")

    args = parser.parse_args()
    return args


#if __name__ == '__main__':
    #main()
