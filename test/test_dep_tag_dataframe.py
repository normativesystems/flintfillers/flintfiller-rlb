from src.flintfiller.chunk_tag_dataframe import pos_dep_tag_text_spacy
import pandas as pd


def test_dep_tag_text_spacy():
    test_sentence_active_1 = pd.Series(['Apple overweegt om voor 1 miljard een U.K. startup te kopen'])
    test_sentence_passive_2 = pd.Series(['Een U.K. startup wordt gekocht door Apple voor 1 miljard'])
    test_sentence_active_3 = pd.Series(['Onze Minister kan het verzoek tot verblijven afwijzen'])
    test_sentence_passive_4 = pd.Series(['Een verblijfverzoek kan afgewezen worden door Onze Minister'])

    result_1 = pos_dep_tag_text_spacy(test_sentence_active_1)
    result_2 = pos_dep_tag_text_spacy(test_sentence_passive_2)
    result_3 = pos_dep_tag_text_spacy(test_sentence_active_3)
    result_4 = pos_dep_tag_text_spacy(test_sentence_passive_4)

    assert result_1 == [[('Apple', 'PROPN', 'nsubj'),
                         ('overweegt', 'VERB', 'ROOT'),
                         ('om', 'ADP', 'mark'),
                         ('voor', 'ADP', 'case'),
                         ('1', 'NUM', 'nummod'),
                         ('miljard', 'NOUN', 'obl'),
                         ('een', 'DET', 'det'),
                         ('U.K.', 'PROPN', 'obj'),
                         ('startup', 'NOUN', 'flat'),
                         ('te', 'ADP', 'mark'),
                         ('kopen', 'VERB', 'ccomp')]]

    assert result_2 == [[('Een', 'DET', 'det'),
                         ('U.K.', 'PROPN', 'nmod'),
                         ('startup', 'NOUN', 'nsubj:pass'),
                         ('wordt', 'AUX', 'aux:pass'),
                         ('gekocht', 'VERB', 'ROOT'),
                         ('door', 'ADP', 'case'),
                         ('Apple', 'PROPN', 'obl:agent'),
                         ('voor', 'ADP', 'case'),
                         ('1', 'NUM', 'nummod'),
                         ('miljard', 'NOUN', 'obl')]]

    assert result_3 == [[('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'nsubj'),
                         ('kan', 'AUX', 'aux'),
                         ('het', 'DET', 'det'),
                         ('verzoek', 'NOUN', 'obj'),
                         ('tot', 'ADP', 'case'),
                         ('verblijven', 'VERB', 'nmod'),
                         ('afwijzen', 'VERB', 'ROOT')]]

    assert result_4 == [[('Een', 'DET', 'det'),
                         ('verblijfverzoek', 'NOUN', 'nsubj:pass'),
                         ('kan', 'AUX', 'aux'),
                         ('afgewezen', 'VERB', 'ROOT'),
                         ('worden', 'AUX', 'aux:pass'),
                         ('door', 'ADP', 'case'),
                         ('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'obl:agent')]]


def test_dep_tag_text_spacy_recipient():
    test_sentence_active_1 = pd.Series(['Apple overweegt om geld aan een startup te geven'])
    test_sentence_passive_2 = pd.Series(['Aan de startup wordt overwogen om geld te geven door Apple'])
    test_sentence_active_3 = pd.Series(['Ik geef aan haar een cadeau'])
    test_sentence_passive_4 = pd.Series(['Aan haar wordt een cadeau gegeven door mij'])

    result_1 = pos_dep_tag_text_spacy(test_sentence_active_1)
    result_2 = pos_dep_tag_text_spacy(test_sentence_passive_2)
    result_3 = pos_dep_tag_text_spacy(test_sentence_active_3)
    result_4 = pos_dep_tag_text_spacy(test_sentence_passive_4)

    assert result_1 == [
        [('Apple', 'PROPN', 'nsubj'),
         ('overweegt', 'VERB', 'ROOT'),
         ('om', 'ADP', 'case'),
         ('geld', 'NOUN', 'obj'),
         ('aan', 'ADP', 'case'),
         ('een', 'DET', 'det'),
         ('startup', 'NOUN', 'obl'),
         ('te', 'ADP', 'mark'),
         ('geven', 'VERB', 'xcomp')]]
    assert result_2 == [
        [('Aan', 'ADP', 'case'),
         ('de', 'DET', 'det'),
         ('startup', 'NOUN', 'obl'),
         ('wordt', 'AUX', 'aux:pass'),
         ('overwogen', 'VERB', 'ROOT'),
         ('om', 'ADP', 'mark'),
         ('geld', 'NOUN', 'obj'),
         ('te', 'ADP', 'mark'),
         ('geven', 'VERB', 'advcl'),
         ('door', 'ADP', 'case'),
         ('Apple', 'PROPN', 'obl')]]

    assert result_3 == [[('Ik', 'PRON', 'nsubj'),
                         ('geef', 'VERB', 'ROOT'),
                         ('aan', 'ADP', 'case'),
                         ('haar', 'PRON', 'obl'),
                         ('een', 'DET', 'det'),
                         ('cadeau', 'NOUN', 'obj')]]
    assert result_4 == [[('Aan', 'ADP', 'case'),
                         ('haar', 'PRON', 'obl'),
                         ('wordt', 'AUX', 'aux:pass'),
                         ('een', 'DET', 'det'),
                         ('cadeau', 'NOUN', 'nsubj:pass'),
                         ('gegeven', 'VERB', 'ROOT'),
                         ('door', 'ADP', 'case'),
                         ('mij', 'PRON', 'obl:agent')]]


def test_tag_no_actor():
    test_sentence_1 = pd.Series(['De startup wordt overgenomen'])

    result_1 = pos_dep_tag_text_spacy(test_sentence_1)

    assert result_1 == [[('De', 'DET', 'det'),
                         ('startup', 'NOUN', 'nsubj:pass'),
                         ('wordt', 'AUX', 'aux:pass'),
                         ('overgenomen', 'VERB', 'ROOT')]]


def test_dep_tag_text_spacy_real_cases():
    test_sentence_1 = pd.Series(
        ['Onze Minister kan vrijstelling dan wel ontheffing verlenen van het eerste en tweede lid aan de vreemdeling'])
    test_sentence_2 = pd.Series([
        'Onze Minister is bevoegd een machtiging tot voorlopig verblijf van de vreemdeling in te trekken of te annuleren'])
    test_sentence_3 = pd.Series([
        'Een terugkeervisum van de vreemdeling kan worden geweigerd door Onze Minister indien de vreemdeling niet door overlegging van documenten aannemelijk heeft gemaakt dat sprake is van een dringende reden die geen uitstel van vertrek mogelijk maakt'])
    test_sentence_4 = pd.Series([
        'Op verzoek van de vreemdeling wordt hem een raadsman toegevoegd door Onze Minister.'])

    result_1 = pos_dep_tag_text_spacy(test_sentence_1)
    result_2 = pos_dep_tag_text_spacy(test_sentence_2)
    result_3 = pos_dep_tag_text_spacy(test_sentence_3)
    result_4 = pos_dep_tag_text_spacy(test_sentence_4)

    assert result_1 == [[('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'nsubj'),
                         ('kan', 'AUX', 'aux'),
                         ('vrijstelling', 'NOUN', 'obj'),
                         ('dan', 'SCONJ', 'advmod'),
                         ('wel', 'ADV', 'advmod'),
                         ('ontheffing', 'NOUN', 'obj'),
                         ('verlenen', 'VERB', 'ROOT'),
                         ('van', 'ADP', 'case'),
                         ('het', 'DET', 'det'),
                         ('eerste', 'ADJ', 'nmod'),
                         ('en', 'CCONJ', 'cc'),
                         ('tweede', 'ADJ', 'conj'),
                         ('lid', 'NOUN', 'obl'),
                         ('aan', 'ADP', 'case'),
                         ('de', 'DET', 'det'),
                         ('vreemdeling', 'NOUN', 'nmod')]]
    assert result_2 == [[('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'nsubj'),
                         ('is', 'AUX', 'cop'),
                         ('bevoegd', 'ADJ', 'ROOT'),
                         ('een', 'DET', 'det'),
                         ('machtiging', 'NOUN', 'obj'),
                         ('tot', 'ADP', 'case'),
                         ('voorlopig', 'ADJ', 'amod'),
                         ('verblijf', 'NOUN', 'nmod'),
                         ('van', 'ADP', 'case'),
                         ('de', 'DET', 'det'),
                         ('vreemdeling', 'NOUN', 'nmod'),
                         ('in', 'ADP', 'compound:prt'),
                         ('te', 'ADP', 'mark'),
                         ('trekken', 'VERB', 'ccomp'),
                         ('of', 'CCONJ', 'cc'),
                         ('te', 'ADP', 'mark'),
                         ('annuleren', 'VERB', 'conj')]]
    assert result_3 == [[('Een', 'DET', 'det'),
                         ('terugkeervisum', 'NOUN', 'nsubj:pass'),
                         ('van', 'ADP', 'case'),
                         ('de', 'DET', 'det'),
                         ('vreemdeling', 'NOUN', 'nmod'),
                         ('kan', 'AUX', 'aux'),
                         ('worden', 'AUX', 'aux:pass'),
                         ('geweigerd', 'VERB', 'ROOT'),
                         ('door', 'ADP', 'case'),
                         ('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'obl:agent'),
                         ('indien', 'SCONJ', 'mark'),
                         ('de', 'DET', 'det'),
                         ('vreemdeling', 'NOUN', 'nsubj'),
                         ('niet', 'ADV', 'advmod'),
                         ('door', 'ADP', 'case'),
                         ('overlegging', 'NOUN', 'obl'),
                         ('van', 'ADP', 'case'),
                         ('documenten', 'NOUN', 'nmod'),
                         ('aannemelijk', 'ADJ', 'compound:prt'),
                         ('heeft', 'AUX', 'aux'),
                         ('gemaakt', 'VERB', 'advcl'),
                         ('dat', 'SCONJ', 'mark'),
                         ('sprake', 'NOUN', 'nsubj'),
                         ('is', 'VERB', 'ccomp'),
                         ('van', 'ADP', 'case'),
                         ('een', 'DET', 'det'),
                         ('dringende', 'ADJ', 'amod'),
                         ('reden', 'NOUN', 'obl'),
                         ('die', 'PRON', 'nsubj'),
                         ('geen', 'DET', 'det'),
                         ('uitstel', 'NOUN', 'obj'),
                         ('van', 'ADP', 'case'),
                         ('vertrek', 'NOUN', 'nmod'),
                         ('mogelijk', 'ADJ', 'xcomp'),
                         ('maakt', 'VERB', 'acl:relcl')]]
    assert result_4 == [[('Op', 'ADP', 'case'),
                         ('verzoek', 'NOUN', 'fixed'),
                         ('van', 'ADP', 'fixed'),
                         ('de', 'DET', 'det'),
                         ('vreemdeling', 'NOUN', 'obl'),
                         ('wordt', 'AUX', 'aux:pass'),
                         ('hem', 'PRON', 'iobj'),
                         ('een', 'DET', 'det'),
                         ('raadsman', 'NOUN', 'nsubj:pass'),
                         ('toegevoegd', 'VERB', 'ROOT'),
                         ('door', 'ADP', 'case'),
                         ('Onze', 'PRON', 'nmod:poss'),
                         ('Minister', 'NOUN', 'obl:agent'),
                         ('.', 'PUNCT', 'punct')]]
