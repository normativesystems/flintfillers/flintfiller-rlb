"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import pandas as pd
import os
import string
import json

from nltk import tokenize
from ast import literal_eval

# import src.flintfiller.chunk_tag_dataframe as chunk_tag_dataframe


from extract_action_sentences import chunk_list_of_dataframes
from extract_action_sentences import retrieve_action_sentences

# Goal: check previously annotated sentences, only add sentences that have not been annotated previously to
# the sampled list of sentences to annotate.

# Output: a list dataframe that can be appended to the list of dataframes from extract_action_sentences.py including
# a column that specifies how often the sentence has been annotated.
cwd = os.getcwd()


# store_dfs = []
# dir_path = os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2021", "all_input_laws_as_csv_2021_merged")
# for i in os.listdir(dir_path):
#     print(type(i))
#     df = pd.read_csv(os.path.join(dir_path, i), sep=";")
#     store_dfs.append(df)
# merged = pd.concat(store_dfs)
# merged.to_csv(os.path.join(dir_path, "input_laws_annotation2021.csv"), index=False, sep=";")

def space_punctuations(text):
    for punctuation in string.punctuation:
        text = text.replace(punctuation, ' ' + punctuation + ' ')
    return text


def preprocess_annotations(filepath):
    """
    :param filepath: path to the location of the annotations
    :return: a list of lists with tokenized sentences
    """
    # alle annotaties
    all_annotations = pd.read_csv(filepath, delimiter=";")

    annotated_tokens = list(all_annotations['tokens'])
    annotated_tokens = [literal_eval(x) for x in annotated_tokens]
    # output a list of strings instead of a list of tokens since its easier to compare for duplicates
    # list of list of tokens to list of strings
    annotated_strings = ["".join(x + " " for x in sentence) for sentence in annotated_tokens]
    all_annotations['tokens_as_string'] = annotated_strings
    return annotated_strings, all_annotations


def preprocess_sentences(filepath):
    """
    :param filepath: path to the location of the previously annotated laws
    :return: a dataframe where each row is a sentence, columns are ['Brontekst', 'law_name', 'tokens']
    """
    # alle wetten die gebruikt zijn voor de annotaties
    previously_annotated_laws = pd.read_csv(filepath, delimiter=";")

    all_sentences_title = []

    for tekst, title, opsomming, opsomming2 in zip(list(previously_annotated_laws['Brontekst']),
                                                   list(previously_annotated_laws['law_name']),
                                                   list(previously_annotated_laws['opsomming']),
                                                   list(previously_annotated_laws['opsomming2'])):
        tekst_split = tokenize.sent_tokenize(tekst, language="dutch")
        if len(tekst_split) == 1:
            all_sentences_title.append((space_punctuations(tekst_split[0]), title, opsomming, opsomming2))
        else:
            for sent in tekst_split:
                all_sentences_title.append((space_punctuations(sent), title, opsomming, opsomming2))

    # make a dataframe to make use of the str.split() function on series of pandas to align this with previous code
    preprocessed_dataframe = pd.DataFrame(all_sentences_title, columns=['Brontekst', 'law_name',
                                                                        "opsomming", "opsomming2"])
    preprocessed_dataframe['tokens'] = preprocessed_dataframe['Brontekst'].str.split()
    preprocessed_dataframe['tokens_as_string'] = ["".join(x + " " for x in sentence) for sentence in
                                                  list(preprocessed_dataframe['tokens'])]

    # later toegevoegd:
    regex_pattern = "artikel.*$|Artikel.*$|artt.*$|hoofdstuk|paragraaf|deel|afdeling"
    # remove string endings such as "artikel 46" or "hoofdstuk 1"
    preprocessed_dataframe['tokens_as_string'] = preprocessed_dataframe['tokens_as_string'].str.replace(regex_pattern,
                                                                                                        '', regex=True)
    # replace two whitespace characters with one. whenever "artikel 46" or "hoofdstuk 1" is placed at the end of a
    # sentence, an additional whitespace character is added. This is removed since it would cause problems with string
    # matching the exact sentence later on.
    preprocessed_dataframe['tokens_as_string'] = preprocessed_dataframe['tokens_as_string'].str.replace(r'\s{2}', ' ',
                                                                                                        regex=True)
    return preprocessed_dataframe


def count_frequency_annotations(list_of_tokens):
    """
    Create a frequency dictionary that counts the annotation frequency of a unique sentence. Sentences with a 1 need
    to be annotated once, since they only appear once in the list of annotations. Sentences with a value of > 1 are
    ready for training, since these appeared atleast twice.
    :param list_of_tokens: a list of tokens as string which are annotated in the previous round.
    :return: frequency dictionary
    """
    frequency_dict = {}

    for annotated_sentence in list(list_of_tokens):
        if annotated_sentence not in frequency_dict:
            frequency_dict[annotated_sentence] = 1
        else:
            frequency_dict[annotated_sentence] += 1
    json.dumps(frequency_dict)
    return frequency_dict


def add_received_annotations(dataframe, frequency_dict):
    """
    Adds a column that keeps track of the amount of times a sentence is annotated in the dataframe of unique sentences.
    :param dataframe:
    :param frequency_dict: frequency dictionary created by count_frequency_annotations function. The keys are sentences
    while the values of the dict store the number of times the sentence is annotated.
    :return:
    """
    store_received_annotations = []
    for sentence in list(dataframe['tokens_as_string']):
        if sentence in list(frequency_dict.keys()):
            n = frequency_dict[sentence]
            store_received_annotations.append(n)
        else:
            store_received_annotations.append(0)

    dataframe['received_annotations'] = store_received_annotations
    return dataframe


def assign_sentences(dataframe: pd.DataFrame, annotated_dataframe: pd.DataFrame):
    """
    Make a split between sentences that have been annotated zero or one times and sentences that have been annotated
    more than once. The sentences with less than 2 annotations are stored together with sentences from other laws that
    do not have sufficient annotations. The sentences with one or more annotations are saved and ready for training.
    :param annotated_dataframe: the dataframe that stores the srl_tags from the annotation round of 2021, file used is
    all_positives_with_tags_full.csv
    :param dataframe: the dataframe with all the unique action sentences.
    :return: stores a dataframe that contains thte annotated srl_tags together with the tokens and the index from the
    all_positives_with_tags_full.csv. Also returns the dataframe with sentences that only have zero or one annotation.
    """
    additional_annotations_req = dataframe[
        (dataframe['received_annotations'] == 0) | (dataframe['received_annotations'] == 1)]
    sufficient_annotations = dataframe[(dataframe['received_annotations'] > 0)]

    store_srl_tags = []
    store_srl_tags_non_action = []
    # check if the action sentence is in the list of annotated sentences, if this is true it means that the previous
    # annotated sentence is considered an action sentence.
    for sentence in annotated_dataframe.itertuples():
        if sentence.tokens_as_string in set(sufficient_annotations["tokens_as_string"]):
            store_srl_tags.append((sentence.srl_tags, sentence.tokens, sentence.Index + 2, sentence.tokens_as_string))
        else:
            print(sentence.tokens_as_string)
            store_srl_tags_non_action.append((sentence.srl_tags, sentence.tokens, sentence.Index + 2,
                                              sentence.tokens_as_string))
    annotations_for_training = pd.DataFrame(store_srl_tags, columns=["srl_tags", "tokens",
                                                                     "original_index_all_positives_with_tags",
                                                                     "tokens_as_string"])
    annotations_not_action = pd.DataFrame(store_srl_tags_non_action, columns=["srl_tags", "tokens",
                                                                              "original_index_all_positives_with_tags",
                                                                              "tokens_as_string"])
    annotations_not_action["received_annotations"] = -1

    # add the counts of each annotation to the sentence for an easy overview
    counts = dict(annotations_for_training["tokens_as_string"].value_counts())
    store_counts = []
    for row in list(annotations_for_training['tokens_as_string']):
        store_counts.append(counts[row])
    annotations_for_training["received_annotations"] = store_counts
    annotations_for_training["received_annotations"] = annotations_for_training["received_annotations"].astype(int)

    annotations_for_training = annotations_for_training.sort_values(by=["received_annotations", "tokens_as_string"],
                                                                    ascending=[False, False])
    # annotations_for_training = annotations_for_training.drop(["tokens_as_string"], axis=1)

    folder_path = os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2021")
    file_path_annotationdata = os.path.join(folder_path, "annotation_data.csv")

    # additional_annotations_req = filter_action_sentences(additional_annotations_req)
    additional_annotations_req.to_csv(file_path_annotationdata, sep=";", index=False)

    file_path_trainingsdata = os.path.join(folder_path, "training_data_2021.csv")
    annotations_for_training.to_csv(file_path_trainingsdata, sep=";", index=False)

    # also save all the annotations to disk
    all_previous_annotations = pd.concat([annotations_for_training, annotations_not_action])
    all_previous_annotations.to_csv(os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2021",
                                                 "all_previous_annotations.csv"), index=False, sep=";")

    return print(f"trainings data from 2021 stored at: {file_path_trainingsdata} \n annotation data stored at: "
                 f"{file_path_annotationdata}")


def prepare_2021_sentences():
    # import previous annotated sentences
    annotated_sentences, annotated_dataframe = preprocess_annotations(os.path.join(cwd, "src", "resource", "data",
                                                                                   "annotations", "annotations_2021",
                                                                                   "all_positives_with_tags_full.csv"))
    frequency_dict = count_frequency_annotations(annotated_sentences)

    # import the laws that were used in the previous annotation round
    previously_annotated_laws = preprocess_sentences(os.path.join(cwd, "src", "resource", "data", "annotations",
                                                                  "annotations_2021",
                                                                  "all_input_laws_as_csv_2021_merged",
                                                                  "input_laws_annotation2021.csv"))

    # chunk
    previously_annotated_laws_chunked = chunk_list_of_dataframes([previously_annotated_laws])
    # its a list of one dataframe
    previously_annotated_laws_chunked = previously_annotated_laws_chunked[0]
    previously_annotated_laws = add_received_annotations(previously_annotated_laws_chunked, frequency_dict)

    # split the dataframe on law name so we can store it seperately, mainly for administrative purposes
    list_of_law_dataframes = [v for k, v in previously_annotated_laws.groupby('law_name')]
    action_sentences = []
    for law in list_of_law_dataframes:
        # reset index so that each sentence_id starts at 0
        law = law.reset_index()
        action_sentence_df = retrieve_action_sentences(law, os.path.join(cwd, "src", "resource", "data",
                                                                         "annotations", "annotations_2022",
                                                                         "action_sentences.xlsx"))
        action_sentence_df = action_sentence_df.loc[(action_sentence_df["presence_of_action_verb"] == "T")]
        action_sentences.append(action_sentence_df)
    merged = pd.concat(action_sentences)
    merged.to_csv(os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2021",
                               "all_action_sentences2021.csv"), sep=";")

    assign_sentences(merged, annotated_dataframe)
    merged = merged.drop(["index", "tokens", "tokens_as_string", "opsomming2", "tags", "verbs",
                          "infinitive_verb", "original_verb", "presence_of_action_verb", "law_name"],
                         axis=1)
    return merged


if __name__ == '__main__':
    prepare_2021_sentences()
