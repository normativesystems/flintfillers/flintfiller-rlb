import tempfile
import pandas as pd

from src.flintfiller.chunk_tag_dataframe import parser

# Uncomment this section if you want to update your regression test files

# def update_expected_spacy_regression():
#     my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")
#     expected_path = "test_data/expected_regression_vreemdelingenwet_spacy.csv"
#     parser("spacy", my_df, expected_path)
#
#
# def update_expected_pattern_regression():
#     my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")
#     expected_path = "test_data/expected_regression_vreemdelingenwet_pattern.csv"
#     parser("pattern", my_df, expected_path)
#
#
# def update_expected_alpino_regression():
#     my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")
#     expected_path = "test_data/expected_regression_vreemdelingenwet_alpino.csv"
#     parser("alpino", my_df, expected_path)
#
#
# update_expected_spacy_regression()
# update_expected_pattern_regression()
# update_expected_alpino_regression()


def test_chunk_tag_dataframe_spacy_regression():
    my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")

    with tempfile.NamedTemporaryFile() as result:
        expected_path = "test_data/expected_regression_vreemdelingenwet_spacy.csv"
        result_path = result.name + ".csv"
        parser("spacy", my_df, result_path)
        for expected_row, new_row in zip(open(expected_path), open(result_path)):
            assert expected_row == new_row


def test_chunk_tag_dataframe_pattern_regression():
    my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")
    with tempfile.NamedTemporaryFile() as result:
        expected_path = "test_data/expected_regression_vreemdelingenwet_pattern.csv"
        result_path = result.name + ".csv"
        parser("pattern", my_df, result_path)
        for expected_row, new_row in zip(open(expected_path), open(result_path)):
            assert expected_row == new_row


def test_chunk_tag_dataframe_alpino_regression():
    my_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet.csv", sep=";")
    with tempfile.NamedTemporaryFile() as result:
        expected_path = "test_data/expected_regression_vreemdelingenwet_alpino.csv"
        result_path = result.name + ".csv"
        parser("alpino", my_df, result_path)
        for expected_row, new_row in zip(open(expected_path), open(result_path)):
            assert expected_row == new_row
