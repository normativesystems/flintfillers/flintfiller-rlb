"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import argparse
import pandas as pd
import re

import flintfiller.chunk_tag_dataframe as chunk_tag_dataframe
import flintfiller.dataframe_to_frame_parser_old_rules as dataframe_to_frame_parser
import flintfiller.dataframe_to_frame_parser_new_rules as dataframe_to_frame_parser_dep
import flintfiller.dataframe_to_frame_parser_new_rules as dataframe_to_frame_parser_dep
from action_filter.extract_action_sentences import read_action_words


def read_csv_to_df(csv_file):
    dataframe = pd.read_csv(csv_file, sep=';')
    return dataframe


def remove_whitespaces_brontekst(dataframe: pd.DataFrame) -> pd.DataFrame:
    # remove whitespace after comma, and remove double whitespaces
    dataframe['Brontekst'] = dataframe['Brontekst'].apply(lambda x: re.sub(r',\s', ',', x))
    dataframe['Brontekst'] = dataframe['Brontekst'].apply(lambda x: re.sub(r'\s{2}', ' ', x))
    return dataframe


def create_output_string(filename: str, selected_parser):
    output_path = filename.split(".")
    output_path = f"{output_path[0]}_{selected_parser}.csv"
    return output_path


def flintfiller():
    args = parse_commandline_arguments()
    selected_parser = args.selected_parser

    if args.df_file and args.postagged_pattern:
        input_df = pd.read_csv(args.df_file, index_col=0, sep=';')
        output_pt = create_output_string(args.postagged_pattern, "pattern")
        chunk_tag_dataframe.parser("pattern", input_df, output_pt)
        if args.flint_output:
            output_flint = args.flint_output
            if not selected_parser == 'spacy':
                input_pt_df = output_pt
                dataframe_to_frame_parser.dataframe_to_frame_parser(input_pt_df, output_flint)
            else:
                # read in the pattern postagged file
                output_postagged_pattern = pd.read_csv(output_pt, sep=";")
                # create the second pos tagged spacy file
                output_postagged_spacy = create_output_string(args.postagged_pattern, 'spacy')
                input_df = remove_whitespaces_brontekst(input_df)
                chunk_tag_dataframe.parser('spacy', input_df, output_postagged_spacy)
                output_postagged_spacy = pd.read_csv(output_postagged_spacy, sep=";")
                # get list of action verbs todo: fix absolute path
                list_action_verbs = read_action_words(
                    "C:\\Users\\bakkerrm\\git\\flintfiller-rlb\\src\\resource\\data\\annotations\\annotations_2022\\action_sentences.xlsx")
                dataframe_to_frame_parser_dep.create_frames_phrase_rule(output_postagged_spacy, output_postagged_pattern
                                                                        , list_action_verbs, output_flint)

            print(f'finished with creating FLINT frames from dataframe, check file: ' + output_flint)
        else:
            print(f'finished with parsing xml to POS tagged dataframe, check file: ' + output_pt)

    elif args.flint_output and args.pt_file and not args.df_file:
        output_flint = args.flint_output
        if not selected_parser == 'spacy':
            pattern_df = args.postagged_pattern
            dataframe_to_frame_parser.dataframe_to_frame_parser(pattern_df, output_flint)

        else:
            # parse met spacy en met pattern
            pattern_df = pd.read_csv(args.postagged_pattern, sep=";")
            spacy_df = pd.read_csv(args.postagged_spacy, sep=";")
            dataframe_to_frame_parser_dep.create_frames_phrase_rule(pattern_df, spacy_df, output_flint)

        print(f'finished with creating FLINT frames from dataframe, check file: ' + output_flint)

    else:
        print('you did not pass the right combination of arguments, parsing not possible.')


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--xml',
                        help="input xml file")
    parser.add_argument('-d', '--dict_file',
                        help="location of file with dictionary of xml")
    parser.add_argument('-df', '--df_file',
                        help="location of file with dataframe of xml")
    parser.add_argument('-ptp', '--postagged_pattern',
                        help="location of dataframe parsed with pattern "
                             "with pos tags or name of to be created dataframe with pos tags")
    parser.add_argument('-pts', '--postagged_spacy',
                        help="location of dataframe parsed with spacy "
                             "with pos tags or name of to be created dataframe with pos tags")
    parser.add_argument('-fo', '--flint_output',
                        help="Output file with Flint frames.")
    parser.add_argument('-sp', '--selected_parser',
                        help="Selected parser for POS and dependency tagging, available options are 'alpino', 'pattern'"
                             "and 'spacy'")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    flintfiller()
