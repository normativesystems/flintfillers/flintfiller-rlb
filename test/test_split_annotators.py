from action_filter.split_annotators import assign_sentences_uneven
from action_filter.split_annotators import assign_sentences_even


def test_assign_sentences_uneven():
    candidates_dict = {"candidate1": 50, "candidate2": 50, "candidate3": 100, "candidate4": 100}
    assigned_sents = assign_sentences_uneven(candidates_dict)

    assert float(sum(list(assigned_sents.values()))) == sum(list(candidates_dict.values())) / 2, \
        "Should be true, the total number of assigned sentences should be equal to the total required annoations " \
        "divided by 2 since each pair gets assigned a sentence effectively counting a 2"
    assert assigned_sents['candidate1-candidate3'] == 25, "Should be true, the intuition is that the annotators that " \
                                                          "annotate a small number of sentences only annotate with " \
                                                          "the annotators that annotate a large number"
    assert assigned_sents['candidate1-candidate4'] == 25, "Should be true, the intuition is that the annotators that " \
                                                          "annotate a small number of sentences only annotate with " \
                                                          "the annotators that annotate a large number"
    assert assigned_sents['candidate2-candidate3'] == 25, "Should be true, the intuition is that the annotators that" \
                                                          " annotate a small number of sentences only annotate with " \
                                                          "the annotators that annotate a large number"
    assert assigned_sents['candidate2-candidate4'] == 25, "Should be true, the intuition is that the annotators that " \
                                                          "annotate a small number of sentences only annotate with " \
                                                          "the annotators that annotate a large number"
    assert assigned_sents['candidate3-candidate4'] == 50, "Should be true, the large number annotators annotate the " \
                                                          "remaining sentences with each other"


def test_assign_sentences_even():
    candidates = ["A", "B", "C", "D", "E"]
    required_sentences = 20
    freq_dict, assigned_ids = assign_sentences_even(candidates, 5, required_sentences)

    sanity_freq_dict = {}
    for i in assigned_ids:
        first_annotator = i.split("-")[0]
        second_annotator = i.split("-")[1]
        if first_annotator not in sanity_freq_dict:
            sanity_freq_dict[first_annotator] = 1
        else:
            sanity_freq_dict[first_annotator] += 1

        if second_annotator not in sanity_freq_dict:
            sanity_freq_dict[second_annotator] = 1
        else:
            sanity_freq_dict[second_annotator] += 1

    for k, v in sanity_freq_dict.items():
        assert k in candidates
        assert v == required_sentences
