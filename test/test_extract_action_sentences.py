import os
import pandas as pd
from ast import literal_eval

from action_filter.extract_action_sentences import check_infinitive
from action_filter.extract_action_sentences import retrieve_action_sentences
from action_filter.filter_action_sentences import read_action_words

cwd = os.getcwd()


def test_check_infinitive():
    action_verbs = read_action_words(os.path.join(cwd, "test_data", "action_sentences.xlsx"))

    example_sentence = pd.DataFrame(["zij nemen de beslissing"], columns=["Brontekst"])
    assert check_infinitive("nemen", example_sentence, action_verbs), "Should be true, nemen in action word list"

    example_sentence2 = pd.DataFrame(["het nemen van de beslissing wordt uitgesteld"], columns=["Brontekst"])
    assert not check_infinitive("nemen", example_sentence2, action_verbs), \
        "Should be false: 'het' + 'nemen' should not be allowed"

    example_sentence3 = pd.DataFrame(["het eten neemt de beslissing"], columns=["Brontekst"])
    assert not check_infinitive("eten", example_sentence3, action_verbs), \
        "Should be false: 'eten' is not in the list of action verbs"

    example_sentence4 = pd.DataFrame(["de nemen van de beslissing wordt uitgesteld"], columns=["Brontekst"])
    assert not check_infinitive("nemen", example_sentence4, action_verbs), \
        "Should be false: 'de' + 'nemen' should not be allowed"


def test_retrieve_action_sentences():
    data = pd.read_csv(os.path.join(cwd, "test_data", "test_input_retrieve_action_sentences.csv"), sep=";")
    data_action_sent = retrieve_action_sentences(data, os.path.join(cwd, "test_data", "action_sentences.xlsx"),
                                                 write_to_disk=False)

    action_verbs = read_action_words(os.path.join(cwd, "test_data", "action_sentences.xlsx"))
    lidwoorden = ["het ", "de ", "een "]

    # double check if sentences that are labelled as action sentences are really actions by looking up the action verb
    # also check for the sentences that are not labelled as action sentences if they are really not actions
    for index, row in data_action_sent.iterrows():
        if row['presence_of_action_verb'] == "T":
            verbs = row['infinitive_verb']
            # check if one or more of the verbs actually exist in the verb list
            store_bools = [x in action_verbs for x in verbs]
            assert any(
                store_bools), "The verbs are not in the action verb list while the row is labelled as an action sent"
        else:
            if not pd.isna(row['infinitive_verb']):
                verbs = literal_eval(row['verbs'])
                verbs = list(verbs.keys())
                # check if there are not occurences of determiner + infinitive, since these do not count as actions,
                # remove if found
                for w in verbs:
                    for lw in lidwoorden:
                        if (lw + w) in row['Brontekst']:
                            verbs.remove(w)
                store_bools = [x not in action_verbs for x in verbs]
                assert all(store_bools), "One or more verbs are in the action word " \
                                         "list while the row is labelled as a non action sent"
