# FlintFiller

This project is created to automatically create Flint Frames. You can choose between two options of creating the 
flint frames, (1) a rule-based approach which uses syntactical tags and rules to define the elements for the frames, 
and (2) a machine learning based approach where a model learns to add semantic role labels to words in a sentence 
(Semantic Role Labelling). This model is then applied on your textual data of choice. This repo contains the code 
for the rule-based approach, if you want to use the SRL method, please go to 
[the FlintFiller-SRL project](https://gitlab.com/normativesystems/flintfillers/flintfiller-srl).

This repository contains:
- A small set of Dutch laws in XML format, already processed and ready to view
- A script to labels to your text of choice

## Table of Contents
- [Usage](#usage)
  - [Getting started](#getting-started)
  - [Using the FlintFiller via the command line](#using-the-flintfiller-via-the-command-line)
  - [Viewing files](#viewing-files)
    - [CSV](#csv)
    - [JSON Flint Frames](#json-flint-frames)
- [Documentation](#documentation)
  - [Juriconnect](#juriconnect)
- [Maintainers](#maintainers)
- [License](#license)


## Usage

If you want to run the code on a new xml, add your xml file and go to the project location on your computer 
and follow the following steps:

### Getting started
- install and activate python 3.6.2*
- install poetry if new to poetry: https://python-poetry.org/docs/#installation
- (`poetry env use /full/path/to/python`)
- `poetry config virtualenvs.in-project true`
- `poetry install`

- Go to project settings of your editor and set the created virtual environment as project interpreter
- **Important:** this tool uses python 3.6 because the pattern package, which is used for the part-of-speech tagging, 
only supports python 3.6.2 Running FlintFiller with a higher python version results in the following error:
```
packages\pattern\text\__init__.py", line 625, in <genexpr>
    dict.update(self, (x.split(" ")[:2] for x in _read(self._path) if len(x.split(" ")) > 1))
RuntimeError: generator raised StopIteration
```

---
\* type `python --version` to check which version of python you are using if you're not sure. If your version is not 
python 3.6.2, you will have to change it. FlintFiller-rlb uses python 3.6.2 and FlintFiller-srl uses python 3.7, so you will 
need different versions of Python. You can either (1) switch between Python versions and then create your environment 
with Poetry, or (2) tell Poetry which version to use for the current project environment:    

1. To switch between Python versions, we recommend the use of 
[pyenv](https://github.com/pyenv-win/pyenv-win) to make your life easier. When installed and working, change the 
python version using pyenv to 3.6.2 There are other approaches such as 
[this one](https://www.freecodecamp.org/news/installing-multiple-python-versions-on-windows-using-virtualenv/),
[this thread](https://stackoverflow.com/questions/52584907/how-to-downgrade-python-from-3-7-to-3-6/65533322), or 
[this thread](https://stackoverflow.com/questions/5087831/how-should-i-set-default-python-version-in-windows), but 
you will need to execute more manual steps. After activating the correct version of Python, follow the steps 
detailed in 'getting started'.    
2. To control the version of Python when creating an environment with Poetry, simply pass another line of code in your
command line, as documented [here](https://python-poetry.org/docs/managing-environments/). To do this, follow the steps 
detailed in 'getting started', including the step in brackets.
---

### Using the FlintFiller via the command line

After installation of the dependencies through poetry you can enter the following example command:

``` bash
python src/main.py 
-x
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/xml_files/BWBR0011823_2019-02-27_Vreemdelingenwet.xml
-d
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/json_files/BWBR0011823_2019-02-27_Vreemdelingenwet.json
-df
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/csv_files/BWBR0011823_2019-02-27_Vreemdelingenwet.csv
-ptp
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/csv_files/postagged/BWBR0011823_2019-02-27_Vreemdelingenwet_pattern.csv
-pts
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/csv_files/postagged/BWBR0011823_2019-02-27_Vreemdelingenwet_spacy.csv
-fo
C:/Users/bakkerrm/git/flintfiller-rlb/src/resource/data/flint_frames/flint_frames_BWBR0011823_2019-02-27_Vreemdelingenwet.json
-sp
"spacy"
```

with arguments:
- `-x`/`--xml` the file location of the input xml file
- `-d`/`--dict_file` the (desired) file location of the dictionary
- `-df`/`--df_file` the (desired) file location of the dataframe
- `-ptp`/`--pt_file` the (desired) file location of the tagged dataframe with pattern
- `-pts`/`--pt_file` the (desired) file location of the tagged dataframe with spacy
- `-fo`/`--flint_output` the (desired) file location of the output file for the flint frames
- `-sp`/`--selected_parser` the parser of choice, options are "spacy", "pattern" or "alpino"

Not all arguments are necessary. For instance, you can pass only an xml and a location for your dictionary file and it will
parse the xml to a dictionary. If you already have a POS tagged dataframe in the right format, you can also pass that,
and pass a location for the flint frames output. 

By running main, you are running both JuriDecompose and FlintFiller. JuriDecompose and FlintFiller are independently 
callable too (with the same arguments as src/main):
``` bash
python flintfiller/flintfiller.py 
python juridecompose/juridecompose.py 
```

For more technical information see the technical-README.md

### Viewing files
#### CSV

To view the CSV files that are parsed from the XML files:
Go to Powershell and the location of your CSV file.
Run the following command:

```
Import-Csv your_file.csv |Out-Gridview
```

You can also import csv data in excel. 


#### JSON Flint Frames

The JSON flint frames can be opened using Visual Studio Code with the FlintEditor plugin.
Otherwise it can be opened using Text Editors such as Notepad++.

## Documentation

- [A FlintFiller report (2020)](./TNO_FlintFiller_report.pdf) (in Dutch only)
- [R.M. Bakker, M.H.T. de Boer, R.A.N. van Drie, and D. Vos (2022). “Extracting Structured Knowledge from Dutch Legal Texts: A Rule-based Approach”. In: Proceedings of KM4LAW. Bolzano, Italy](https://ceur-ws.org/Vol-3256/km4law1.pdf).

#### Juriconnect

In the CSV you find the juriconnect columns, jci1.0 and jci1.3. a juriconnect reference is built like this: 

```
    jci1.3:{type}:{BWB-nummer}{key-value paar}*
```

For more information see https://www.forumstandaardisatie.nl/sites/bfs/files/Juriconnect_Standaard_BWB_1_3_1.pdf.

## Maintainers
Roos Bakker, TNO (roos.bakker@tno.nl)

Romy van Drie, TNO (romy.vandrie@tno.nl)

Daan Vos, TNO (daan.vos@tno.nl)

Maaike de Boer, TNO (maaike.deboer@tno.nl)

## License

FlintFiller is released under Apache 2.0, for more information see the LICENSE.

