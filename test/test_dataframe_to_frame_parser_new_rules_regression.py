import tempfile
import os

from src.flintfiller.dataframe_to_frame_parser_new_rules import create_frames_phrase_rule
from src.flintfiller.dataframe_to_frame_parser_new_rules import read_csv_to_df
from src.flintfiller.dataframe_to_frame_parser_new_rules import read_action_words
cwd = os.getcwd()


list_of_action_verbs = read_action_words(os.path.join(cwd, "test_data", "action_sentences.xlsx"))


def test_dataframe_to_frame_parser_new_rules_phrase_rule_regression():
    expected_flintframe = "test_data/test_frame_phrase_rule_vreemdelingenwet.json"

    pos_dep_tagged_csv = "test_data/BWBR0011823_2019-02-27_Vreemdelingenwet_spacy.csv"
    pos_dep_tagged_csv = read_csv_to_df(str(pos_dep_tagged_csv))
    pos_chunk_tagged_csv = "test_data/BWBR0011823_2019-02-27_Vreemdelingenwet_pattern.csv"
    pos_chunk_tagged_csv = read_csv_to_df(str(pos_chunk_tagged_csv))

    with tempfile.NamedTemporaryFile() as result:
        result_path = result.name + ".json"
        create_frames_phrase_rule(pos_dep_tagged_csv, pos_chunk_tagged_csv, list_of_action_verbs, result_path)
        for expected_row, new_row in zip(open(expected_flintframe), open(result_path)):
            assert expected_row == new_row
