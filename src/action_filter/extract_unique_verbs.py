"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import os
import pandas as pd
import argparse
from ast import literal_eval
from typing import Tuple

from extract_action_sentences import xml_to_dataframe_eas
from extract_action_sentences import chunk_list_of_dataframes



def retrieve_unique_verbs(dataframe: pd.DataFrame) -> Tuple[list, list]:
    """
    Retrieve unique verbs in two-fold.

    :param dataframe: containing i.a. "brontekst" and "verbs" columns
    :return: 1. store_unique_verbs_base: a list of base verbs i.e. "aanhalen"
             2. store_unique_verbs: a list of conjugated verbs i.e. "aangehaald"
    """

    store_unique_verbs_base = []
    store_unique_verbs = []
    for index, row in dataframe.iterrows():
        verb_dict = literal_eval(str(row['verbs']))
        # i.e. verb_base = aanhalen, verb = aangehaald
        for verb_base, verb in verb_dict.items():
            if verb not in store_unique_verbs:
                store_unique_verbs_base.append(verb_base)
                store_unique_verbs.append(verb)
    return store_unique_verbs_base, store_unique_verbs


def include_dutch_verb_col(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    Open common dutch wordlist retrieved from below. Includes a column that indicates whether a verb exists in
    the word list. Wordlist found at this location:
     https://raw.githubusercontent.com/OpenTaal/opentaal-wordlist/master/wordlist.txt

    :param dataframe: containing i.a. "brontekst" and "verbs" columns
    :return: dataframe with included "OOV" column
    """
    with open(os.path.join("data", "dutch_word_list", "wordlist.txt")) as f:
        dictionary = f.read().splitlines()

    dataframe['OOV'] = dataframe['base'].apply(lambda x: x in dictionary)
    return dataframe


def retrieve_verb_example_sentence(dataframe):
    """
    Loops through the 'Brontekst' column and adds uniquely found verbs to a dictionary together with the brontekst
    sentence. Then this dictionary is transformed into a dataframe and saved to disk.
    :param dataframe:
    :return: dataframe included with unique verbs in base and conjugated, an example sentence and a boolean value
    column that indicates whether the given verb exists in a dutch wordlist
    """
    unique_verbs_base, unique_verbs = retrieve_unique_verbs(dataframe)
    # make a dictionary with key: verb, value: example sentence, to check whether the verb exist already
    examples = {}
    for verb in unique_verbs:
        for bron_tekst in list(dataframe['Brontekst']):
            if verb in bron_tekst and verb not in list(examples.keys()):
                examples[verb] = bron_tekst
    # transform dict into dataframe and save
    df = pd.DataFrame(list(zip(list(unique_verbs_base), list(examples.keys()), list(examples.values()))),
                      columns=['base', 'conjugated', 'example_sentence'])
    # only keep unique base verbs, necessary since the code looks for conjugate verbs & extracts sentences based on this
    df = df.drop_duplicates(subset=['base'])
    # add a column that indicates whether a verb exist in a dutch wordlist
    df = include_dutch_verb_col(df)

    args = parse_commandline_arguments()
    output_df = args.df_file

    df.to_csv(output_df, sep=";")
    return print("dataframe created at {}".format(output_df))


def main():
    # list of data frame(s)
    law_data_frames = xml_to_dataframe_eas()
    # open gdpr file seperately since its structured different than the usual laws and add to list of law data frames
    gdpr = pd.read_csv(os.path.join("data", "csv_files", "gdpr-nl.csv"), sep=';')
    gdpr['law_name'] = str("gdpr")
    law_data_frames.append(gdpr)
    # add pos tags and infinitives to the dataframes
    law_data_frames_chunk_pos = chunk_list_of_dataframes(law_data_frames)
    # concate the list of dataframes together
    merged_dataframes = pd.concat(law_data_frames_chunk_pos)
    retrieve_verb_example_sentence(merged_dataframes)
    return print("iteration complete")

# of voor 1 xml file
# python src/extract_unique_verbs.py -x data/law_folder_dv/BWBR0001840_2018-12-21_Grondwet.xml -d data/output_folder_dv/placeholder.json -df data/output_folder_dv/sampled_sentences.csv


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--xml',
                        help="input xml file")
    parser.add_argument('-d', '--dict_file',
                        help="location of file with dictionary of xml")
    parser.add_argument('-df', '--df_file',
                        help="location of file with dataframe of xml")
    parser.add_argument('-lf', '--law_folder',
                        help="location of law folder containing laws as xml or json.")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
