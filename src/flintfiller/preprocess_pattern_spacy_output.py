"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

from itertools import zip_longest


def reformat_pattern_chunk(pattern_chunk: list) -> list:
    """
    Transform the output of a sentence parsed by pattern which is a list of tuples ordered by chunks
    to a list of lists.
    :param pattern_chunk: list of lists structured as a list of with tuples: i.e. [chunk, (token, pos), (token, pos)]
    :return: list of lists structured as [[token, pos_tag, chunk], ..]
    """
    reformatted_sentence = []
    cntr = 0
    for list_of_objects in pattern_chunk:
        # the chunk is always the first item in the list_of_objects
        current_chunk = f"{list_of_objects[0]}{cntr}"
        # we ignore the chunk
        for token, pos_tag in list_of_objects[1:]:
            reformatted_token = (token, pos_tag, current_chunk)
            reformatted_sentence.append(reformatted_token)
        cntr += 1
    return reformatted_sentence


def drop_non_matching_tokens(spacy_list: list, pattern_list: list) -> list:
    """
    Create a new list that contains the tokens from the list of spacy objects which is structured as: [[tokens, pos, dep], ..]
    Loop through the list of pattern objects and match the tokens in the pattern list with the spacy tokens. If the
    exact pattern token is in the list of spacy tokens, store the entire list of pattern objects in a new list.

    :param spacy_list: list of list of objects: [[tokens, pos, dep], ..]
    :param pattern_list: list of list of objects: [[tokens, pos, chunk], ..]
    :return: list of list of objects: [[tokens, pos, chunk], ..] with ONLY tokens that are also in the spacy list.
    """
    list_of_spacy_tokens = [x[0] for x in spacy_list]
    new_lst = []
    for list_of_objects in pattern_list:
        if list_of_objects[0] in list_of_spacy_tokens:
            new_lst.append(list_of_objects)
    return new_lst


def combine_pattern_spacy(pattern_list: list, spacy_list: list) -> list:
    """
    Pattern ignores certain words which results in the spacy and pattern output not being the same length.
    This function ensures that they are of the same length by recursively iterating over both lists and adding a
    placeholder chunk in case a word is missing.
    :param pattern_list: list of lists structured as [[token, pos_tag, chunk], ..]
    :param spacy_list: list of tuples structured as [[token, pos_tag, dep], ..]
    :return: a list of lists containing all words of a sentence structured as [[token, pos, chunk], ..]
    """
    cntr = -1

    for pattern_output, spacy_output in zip_longest(pattern_list, spacy_list, fillvalue="PLCHLDR"):
        cntr += 1
        if pattern_output[0] == spacy_output[0]:
            pass
        else:
            pattern_list.insert(cntr, [spacy_output[0], "PLACEHOLDER_POS", f"PLACEHOLDER_CHUNK_{cntr}"])
            if cntr > len(spacy_list) + 40:
                spacy_tokens = [x[0] for x in spacy_list]
                pattern_tokens = [x[0] for x in pattern_list]
                # never_combined_list.append((sentence_cntr, spacy_tokens, pattern_tokens))
                break
            return combine_pattern_spacy(pattern_list, spacy_list)
    return pattern_list
