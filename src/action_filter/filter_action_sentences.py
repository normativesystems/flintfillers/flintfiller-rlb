"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import pandas as pd
import os
import re
import spacy

# from extract_action_sentences import read_action_words

nlp = spacy.load("nl_core_news_lg")


# action_sentence_list = open(os.path.abspath(os.path.join("src", "resource", "data", "annotations",
#                                                          "annotations_2022", "action_words.txt")), "r").readlines()
# action_sentence_list = [x.rstrip() for x in action_sentence_list]
def read_action_words(filepath):
    """
    Reads and selects the action verbs that are labelled as action by selecting only the rows that have "ja" in the
    "action" column.
    :param filepath: location of the excel sheet.
    :return: list of the base of the action verbs.
    """
    action_words_excel = pd.read_excel(filepath, sheet_name="action_sentences_robert_oov3", engine='openpyxl')
    action_words_excel = action_words_excel[action_words_excel["action"] == "ja"]
    action_words = list(action_words_excel['base'])
    return action_words


def remove_unwanted_str_endings(dataframe: pd.DataFrame) -> pd.DataFrame:
    """
    Remove unwanted article, paragraph and chapter numbers at the end of strings in the "Brontekst" column, i.e.:
    Bij de algemene maatregel van bestuur kunnen (..) toepassing worden verklaard . hoofdstuk I , titel I
    Bij of krachtens algemene maatregel van rijksbestuur kunnen (..) en  vergoeding van hun onkosten . artikelen 22A

    :param dataframe:
    :return: dataframe with adjusted "Brontekst" column
    """
    regex_pattern = "artikel.*$|Artikel.*$|artt.*$|hoofdstuk|paragraaf|deel|afdeling"
    processed_sentences = []

    for index, row in dataframe.iterrows():

        sent = str(row["Brontekst"])
        match = re.search(regex_pattern, str(sent))
        if not sent.rstrip().endswith(".") and not sent.rstrip().endswith(";") and \
                not sent.rstrip().endswith(":") and match:
            processed_sentence = re.sub(regex_pattern, '', sent)
            processed_sentences.append(processed_sentence)
        else:
            processed_sentences.append(sent)

    dataframe["preprocessed_sentences"] = processed_sentences

    dataframe = dataframe.drop('Brontekst', 1)
    dataframe = dataframe.rename({'preprocessed_sentences': 'Brontekst'}, axis='columns')
    return dataframe


def is_unsolved_reference(row: pd.Series):
    """
    Filter out sentences that have a reference to a article that is not available in the sentence. This is achieved by
    checking sentences that have additional whitespace characters, i.e.:
    "In afwijking van de  en  is  ", "Een aanwijzing op grond van  ,   ,  of op grond van  , "
    Also filter sentences with multiple comma's in succesion:
    "Besluiten die zijn genomen op grond van de  ,   ,   ,   ,   ,   ,   ,   ,  "
    :param row:
    :return: True if an unsolved reference is found, otherwise False is returned.
    """
    match = re.search("(de|het|een|en)\s\s+", str(row["Brontekst"]))
    match_consecutive_comma = re.search(",\s\s\s,", str(row["Brontekst"]))
    if match or match_consecutive_comma:
        unsolved_reference = True
    else:
        unsolved_reference = False
    return unsolved_reference


def check_if_definition(row: pd.Series) -> bool:
    """
    Evaluates a sentence threefold 1) look for rows that end with either ";" or ":", 2) look for artikelnumbers such as
    ["Artikel1a:1", "Artikel1.2", "Artikel1:5", "Artikel 1"], but not: "Artikel11" and 3) look for
     sentences that have ";" or ":" at the beginning.
    :param row: iterable row that is created by dataframe.iterows(), this function uses row["opsomming"] and
    row["Brontekst"]
    :return: a boolean that is true when the input sentence is a definition because one of the evaluations is true,
    when false the sentence is not a definition.
    """
    if str(row["Brontekst"]).rstrip().endswith(';') or str(row["Brontekst"]).rstrip().endswith(':'):
        ends_with_colon = True
    else:
        ends_with_colon = False

    match = re.match("Artikel1+\D", str(row["opsomming"]))
    if bool(match) or str(row["opsomming"]) == "Artikel1":
        in_first_article = True
    else:
        in_first_article = False

    tokens = str(row["Brontekst"]).split(" ")
    if (";" or ":") in tokens[0:3]:
        colon_first_tokens = True
    else:
        colon_first_tokens = False

    # only if all values are False, it is not a definition
    bool_list = [ends_with_colon, in_first_article, colon_first_tokens]

    if any(bool_list):
        sentence_is_definition = True
    else:
        sentence_is_definition = False
    return sentence_is_definition


def check_action_verb_root_and_copula(row: pd.Series, action_verbs):
    """
    Check if the verb of a sentence is the root using SpaCy and whether this verb is in the action sentence list.
    Also check whether there exist a copula (koppelwerkwoord) before the root verb which is in the action sentence list.
    :param row:
    :param action_verbs: list of the action verbs
    :return: a boolean that is True when a verb is found that is the root which is also in the action_verbs,
    return False otherwise. a boolean that is True when a copula is found before the root, return False otherwise.
    """
    doc = nlp(row["Brontekst"])
    action_verb_is_root = False
    copula_before_root = False
    index = 0

    for n in doc:
        if n.dep_ == 'ROOT' and n.pos_ == 'VERB' and n.lemma_ in action_verbs:
            action_verb_is_root = True

        if index > 0:
            previous_n = doc[index - 1]
            previous_dep = previous_n.dep_
            if n.dep_ == 'ROOT' and n.pos_ == 'VERB' and n.lemma_ in action_verbs and previous_dep == 'cop':
                copula_before_root = True
        index += 1
    return action_verb_is_root, copula_before_root


def filter_action_sentences(dataframe: pd.DataFrame, action_verb_path) -> pd.DataFrame:
    """
    Only select action sentences that start with a capital letter, are a not a definition (see check_if_definition),
    are not an unsolved reference (see is_unsolved_reference), have a root which is an action verb and do not have a
    copula before this action verb root (see check_action_verb_root_and_copula).
    :param dataframe:
    :param action_verb_path: path to the file that contains the list of action verbs
    :return: a dataframe with sentences that adhere to the specified constraints. each check is added as a specific
    column to count the distribution of True and False values.
    """
    action_sentence_list = read_action_words(action_verb_path)

    row_is_title = []
    row_is_definition = []
    row_is_us_reference = []
    verb_is_root = []
    copula_before_verb = []

    for index, row in dataframe.iterrows():
        tokens = row["Brontekst"].split(" ")

        if tokens[0].istitle():
            row_is_title.append("T")
        else:
            row_is_title.append("F")

        if not check_if_definition(row):
            row_is_definition.append("T")
        else:
            row_is_definition.append("F")

        if not is_unsolved_reference(row):
            row_is_us_reference.append("T")
        else:
            row_is_us_reference.append("F")

        check_root, check_copula = check_action_verb_root_and_copula(row, action_sentence_list)
        if check_root:
            verb_is_root.append("T")
        else:
            verb_is_root.append("F")

        if check_copula:
            copula_before_verb.append("T")
        else:
            copula_before_verb.append("F")

    dataframe["row_is_title"] = row_is_title
    dataframe["row_is_not_definition"] = row_is_definition
    dataframe["row_is_not_us_reference"] = row_is_us_reference
    dataframe["verb_is_root"] = verb_is_root
    dataframe["copula_before_verb"] = copula_before_verb

    correct_action_sentences = dataframe[(dataframe["row_is_title"] == "T") &
                                         (dataframe["row_is_not_definition"] == "T") &
                                         (dataframe["row_is_not_us_reference"] == "T") &
                                         (dataframe["verb_is_root"] == "T") &
                                         (dataframe["copula_before_verb"] == "F")]
    return correct_action_sentences


if __name__ == '__main__':
    cwd = os.getcwd()

    example_df = pd.read_csv(os.path.join(cwd, "src", "resource", "data",
                                          "annotations", "annotations_2022", "test_action_filter_main.csv"), sep=";")
    filtered_df = filter_action_sentences(example_df, os.path.abspath(os.path.join("src", "resource", "data",
                                                                                   "annotations", "annotations_2022",
                                                                                   "action_sentences.xlsx")))
    filtered_df.to_csv(os.path.join(cwd, "src", "resource", "data", "annotations", "annotations_2022",
                                    "annotation_filtered.csv"), sep=";", index=False)
