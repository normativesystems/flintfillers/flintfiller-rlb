"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import random
import pandas as pd
import os
import math
from itertools import combinations
from collections import defaultdict

cwd = os.getcwd()


def assign_sentences_uneven(candidates: dict):
    """
    The intuition behind assigning sentences when the number of sentences that each annotator will anotate varies is
    the following: annotators that annotate the minimum amount will only annotate with annotators that annotate the
    maximum amount.
    First, define the maximum- and minimum amount of sentences that will be annotated, which is retrieved from the
    input candidate list. Also store how often these values occur in a frequency dictionary.
    Then look up how much sentences the group that will annotate the minimum value of sentences will annotate.
    :param candidates: A dictionary containing a unique user identifier as key and the planned number of sentences to
    annotate.
    :return:
    """
    max_value = max(list(candidates.values()))
    min_value = min(list(candidates.values()))

    # track how often the max and min value occur
    freq_dict = {}
    for k, v in candidates.items():
        if v not in freq_dict:
            freq_dict[v] = 1
        else:
            freq_dict[v] += 1

    # Determine how much the
    # The annotators that annotate the small number of sentences only annotate together with the annotators that annotate
    # the large amount
    total_min_annotators = freq_dict[min_value]
    total_max_annotators = freq_dict[max_value]
    # the amount per small-large candidate pair is the total number / the number of large annotators
    # (i.e. 50 / 3 = [17, 17, 16]
    amount_to_annotate_small = (
    [min_value // total_max_annotators + (1 if x < min_value % total_max_annotators else 0) for x in
     range(total_max_annotators)])

    # store all id's of the small- and large annotators.
    small_annotators = [k for k, v in candidates.items() if v == min_value]
    large_annotators = [k for k, v in candidates.items() if v == max_value]

    # store the annotation pairs and their required amount of sentences to annotate in a dictionary
    store_pairs_req_annotations = {}
    iteration_counter = 0

    # pair the low amount of annotators only with high amount of annotators.
    for small_annotator in small_annotators:
        for large_annotator in large_annotators:
            annotation_pair = str(small_annotator) + "-" + str(large_annotator)

            required_annotations = amount_to_annotate_small[iteration_counter]
            iteration_counter += 1

            store_pairs_req_annotations[annotation_pair] = required_annotations

        # each iteration move the contents of the required annotation list by 1, to automatically ensure that uneven
        # numbers get taken care off (only relevant if individual annotations / max value annotators does not result in
        # an equal split (i.e. 50/3 = [17, 17, 16] instead of 51/3 = 17.
        amount_to_annotate_small = [amount_to_annotate_small[-1]] + amount_to_annotate_small[:-1]

        iteration_counter = 0

    # pair the remaining annotators with each other
    possible_combinations_high_users = ["-".join(map(str, comb)) for comb in combinations(large_annotators, 2)]

    # the remaining amount / the total numbers of annotators that annotate the maximum amount -1 (since you dont
    # annotate with yourself)
    amount_to_annotate_large = int(
        math.floor((max_value - (total_min_annotators * amount_to_annotate_small[0])) / (len(large_annotators) - 1)))

    # assing this to each pair
    for pair in possible_combinations_high_users:
        store_pairs_req_annotations[pair] = amount_to_annotate_large
    return store_pairs_req_annotations


def assign_sentences_to_annotators(required_annotations: dict, dataframe: pd.DataFrame):
    all_pairs = []

    for candidate_pair, n in required_annotations.items():
        # add all pairs to a list
        c1 = candidate_pair.split("-")[0]
        c2 = candidate_pair.split("-")[1]
        candidate_list = [c1, c2]
        pairs = [candidate_list] * n
        all_pairs = all_pairs + pairs

    # shuffle the list of pairs
    random.shuffle(all_pairs)

    # slice the required amount of sentences
    required_sentences = dataframe[:len(all_pairs)]
    # drop old annotator_id column
    # dataframe = dataframe.drop(columns=['annotator_ID'])
    required_sentences['annotator_ID'] = all_pairs
    return required_sentences


def assign_sentences_even(candidates: list, n_annotators: int, n_sentences: int):
    """
    Create all possible combinations of unique annotation couples
    and add these to a frequency dictionary (i.e. "A-B", "B-C").
    Finally, loop through the list of sentences and assign the lowest frequency pair to each sentence.
    :param candidates: a list of candidates, i.e. ["A", "B", "C", "D", "E", "F"]
    :param n_sentences: the number of sentences each annotator will annotate
    :param n_annotators: the number of annotators available.
    The total number of required sentences is (n_annotators / 2) * n_sentences because each sentence will be annotated
    twice.
    :return: a frequency dictionary where the number of assigned sentences is stored as value and the keys are the
    annotation pairs and a list of the assigned IDs.
    """
    unique_sentences = (n_annotators / 2) * n_sentences

    # create a frequency dictionary to store the counts of pairs
    freq_dict = defaultdict(int)
    # create a frequency dictionary to store individual counts

    # https://stackoverflow.com/questions/18201690/get-unique-combinations-of-elements-from-a-python-list
    possible_combinations = ["-".join(map(str, comb)) for comb in combinations(candidates, 2)]
    for combination in possible_combinations:
        freq_dict[combination]

    assigned_ids = []
    for x in range(int(unique_sentences)):
        selected_pair = min(freq_dict, key=freq_dict.get)
        assigned_ids.append(selected_pair)
        freq_dict[selected_pair] += 1
    return freq_dict, assigned_ids


# 16-5 assign sentences to Randstad mensen
# preparing the first round of Randstad annotations took more effort since we had to include the control sentences


def read_and_split_df(path_to_annotations):
    """
    Split the dataframe into two parts: 1) rows that have been annotated ONCE, 2) rows that have been annotated ZERO
    :param path_to_annotations:
    :return: Annotations that have been annotated once and twice
    """
    remaining_sentences_wo_control_sentences = pd.read_csv(path_to_annotations, sep=";")
    # split the dataframe into two parts: 1) rows that have been annotated ONCE, 2) rows that have been annotated ZERO

    remaining_annotations_1 = remaining_sentences_wo_control_sentences.loc[
        remaining_sentences_wo_control_sentences['received_annotations'] == 1]
    remaining_annotations_0 = remaining_sentences_wo_control_sentences.loc[
        remaining_sentences_wo_control_sentences['received_annotations'] == 0]
    print(f"len 1 annotation: {len(remaining_annotations_1)}, len 0 annotations: {len(remaining_annotations_0)}")
    return remaining_annotations_1, remaining_annotations_0


def assign_sentences_2_annotations(remaining_annotations_0, list_of_candidates):
    """
    Assign the sentences that have not been annotated before. Now we assign 800 - (310 / 4) - 30 of the dataframe with
    0 annotations. We assign (310 / 4) because we need to assign the sentences that have 1 annotations from previous
    annotation rounds into account. We assign - 30 because of the 30 control sentences.
    :param remaining_annotations_0:
    :param list_of_candidates: list of names that will be assigned
    :return:
    """
    freq_dict_0_annotations, assigned_ids_0_annotations = assign_sentences_even(list_of_candidates, 4, 693)
    assigned_sentences_randstad = assign_sentences_to_annotators(freq_dict_0_annotations, remaining_annotations_0)
    return assigned_sentences_randstad


def assign_sentences_1_annotations(remaining_annotations_1, list_of_candidates):
    """
    Assign the sentences have one annotation from previous rounds. In total there were 310 such annotations, for this
    reason we assign (310 / 4 (the number of annotators) to each annotator.
    :param remaining_annotations_1:
    :param list_of_candidates:
    :return:
    """
    remaining_annotations_1_ids = []
    for annotator_name in list_of_candidates:
        annotator_lst = [annotator_name] * 77
        remaining_annotations_1_ids = remaining_annotations_1_ids + annotator_lst
    random.shuffle(remaining_annotations_1_ids)
    # to get an even number, assign the 308 annotator names to the (310 - 2) sentences that have been annotated once
    remaining_annotations_1 = remaining_annotations_1[:-2]
    remaining_annotations_1['annotator_id'] = remaining_annotations_1_ids
    return remaining_annotations_1


def merge_assigned_sentences(assigned_sentences_randstad, remaining_annotations_1):
    """
    Create a dictionary with the annotator as key and (sentence_id, brontekst_sentence) tuple pairs.
    we need to fill the dict with 1) the sentences from the 0 annotations df and 2) the sentences from the 1
    annotation df.
    :param assigned_sentences_randstad:
    :param remaining_annotations_1:
    :return: annotator_dict: a dictionary with the authors as key and a list of sentence_dos
    """
    annotator_dict = defaultdict(list)
    for row in assigned_sentences_randstad.itertuples():
        first_annotator = row.annotator_ID[0]
        second_annotator = row.annotator_ID[1]

        annotator_dict[first_annotator].append((row.sentence_id, row.brontekst))
        annotator_dict[second_annotator].append((row.sentence_id, row.brontekst))

    for row in remaining_annotations_1.itertuples():
        annotator_dict[row.annotator_id].append((row.sentence_id, row.brontekst))
    return annotator_dict


def import_control_sentences(path_to_file):
    control_sentences = pd.read_csv(path_to_file, sep=";")
    control_sentences = control_sentences[:-1]
    return control_sentences


def include_control_sentences(annotator_dict, control_sentences):
    """

    :param annotator_dict:
    :param control_sentences:
    :return:
    """
    # now we can fill the list with at each 50th sentence two control sentences
    # >>> waarom elke 53 ipv 50 > modulo start bij 0
    final_annotation_list = []

    for annotator_name, sentence_tuple in annotator_dict.items():
        sentence_counter = 1
        control_sentence_counter = 0
        for sentence_id, sentence in sentence_tuple:
            if sentence_counter % 50 == 0 and sentence_counter < 800:
                first_ctrl_sent = [annotator_name, control_sentences['sentence_id'].iloc[control_sentence_counter],
                                   control_sentences['brontekst'].iloc[control_sentence_counter], True]
                second_ctrl_sent = [annotator_name, control_sentences['sentence_id'].iloc[control_sentence_counter + 1],
                                    control_sentences['brontekst'].iloc[control_sentence_counter + 1], True]
                final_annotation_list.append(first_ctrl_sent)
                final_annotation_list.append(second_ctrl_sent)

                # we still want to add the current sentence
                lst = [annotator_name, sentence_id, sentence, False]
                final_annotation_list.append(lst)

                control_sentence_counter += 2
            else:
                lst = [annotator_name, sentence_id, sentence, False]
                final_annotation_list.append(lst)
            sentence_counter += 1
    final_annotation_df = pd.DataFrame(final_annotation_list, columns=['annotator_id', 'sentence_id', 'brontekst',
                                                                       'is_test_sentence'])
    return final_annotation_df


def assign_sentences_ole(control_sentences):
    # add control sentences to the list of list
    control_sentences_copy = control_sentences
    ole_annotators = ["Maaikdebo", "Romyvandr", "Roosbakker", "Ioannisto", "Erikboertj", "Robertdoes", "Daanvos"]

    duplicated_control_sentences = []
    for x in range(7):
        duplicated_control_sentences.append(control_sentences_copy)

    merged_control_sentences = pd.concat(duplicated_control_sentences)

    ole_annotators_lst = []
    for x in ole_annotators:
        single_annotator_lst = [x] * 30
        ole_annotators_lst = ole_annotators_lst + single_annotator_lst

    merged_control_sentences['annotator_id'] = ole_annotators_lst
    merged_control_sentences['is_test_sentence'] = True
    return merged_control_sentences


def prepare_first_round_randstad():
    remaining_annotations_1, remaining_annotations_0 = read_and_split_df(os.path.join(cwd, "src", "resource", "data",
                                                                                      "annotations", "annotations_2022",
                                                                                      "all_remaining_sent_no_id_romy.csv"))
    ctrl_sentences = import_control_sentences(os.path.join(cwd, "src", "resource", "data", "annotations",
                                                           "annotations_2022", "control_sentences.csv"))
    assigned_sentences_randstad = assign_sentences_2_annotations(remaining_annotations_0,
                                                                 ["Anthoinettehelm", "Davidthies", "Roosvandert",
                                                                  "Manoukbea"])
    remaining_annotations_1 = assign_sentences_1_annotations(remaining_annotations_1,
                                                             ["Anthoinettehelm", "Davidthies", "Roosvandert",
                                                              "Manoukbea"])

    annotator_dict = merge_assigned_sentences(assigned_sentences_randstad, remaining_annotations_1)

    randstad_annotation_df = include_control_sentences(annotator_dict, ctrl_sentences)
    merged_control_sentences = assign_sentences_ole(ctrl_sentences)
    final_annotation_df_incl_ole = pd.concat([randstad_annotation_df, merged_control_sentences])
    return final_annotation_df_incl_ole
