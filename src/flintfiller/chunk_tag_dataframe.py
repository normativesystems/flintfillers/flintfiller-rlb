"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import os
import pandas as pd
from nltk.corpus import alpino
from nltk.tag import UnigramTagger, BigramTagger
from pattern.text.nl import parse, split, conjugate
import spacy

spacy_model = spacy.load("nl_core_news_lg")


def train_dutch_tagger():
    training_corpus = alpino.tagged_sents()
    unitagger = UnigramTagger(training_corpus)
    bitagger = BigramTagger(training_corpus, backoff=unitagger)
    pos_tag = bitagger.tag
    return pos_tag


def read_csv_to_df(csv_file):
    dataframe = pd.read_csv(csv_file, sep=';')
    print("csv loaded from " + csv_file)
    return dataframe


def write_df_to_csv(dataframe, file):
    dataframe.to_csv(file, sep=";", index=False)
    print("dataframe with POS tags written to " + file)


def pos_dep_tag_text_spacy(text_column: pd.Series) -> list:
    tagged_text = []
    for sentence in text_column:
        tagged_sentence = []
        spacy_doc = spacy_model(sentence)
        for token in spacy_doc:
            pos_dep_tagged_token = token.text, token.pos_, token.dep_
            tagged_sentence.append(pos_dep_tagged_token)
        tagged_text.append(tagged_sentence)
    return tagged_text


def pos_tag_text_alpino(text_column: pd.Series, pos_tag) -> list:
    parsed_text = []
    for sentence in text_column:
        pos_tagged_text = pos_tag(sentence.split())
        parsed_text.append(pos_tagged_text)
    return parsed_text


def parse_text_pattern(text_column: pd.Series) -> list:
    parsed_text = []
    parsed_verbs = []

    for artikel in text_column:
        list_per_artikel = []
        verbs_per_artikel = {}
        if isinstance(artikel, str):
            s = parse(artikel)
            for pos_tagged_text in split(s):
                for chunk in pos_tagged_text.chunks:
                    list_per_artikel.append([chunk.type] + [(w.string, w.type) for w in chunk.words])
                    # add the infinitive form of the words as 'verbs'
                    for words in chunk.words:
                        if 'VB' in words.type:
                            verbs_per_artikel[conjugate(str(words), 'INFINITIVE')] = str(words)
        # else:
        #     print('Dit artikel bevat geen text')
        parsed_text.append(list_per_artikel)
        parsed_verbs.append(verbs_per_artikel)

    return parsed_text, parsed_verbs


def write_parsed_text_to_file(parsed_text, file):
    with open(str(file), 'w') as f:
        for sentence in parsed_text:
            f.write("%s\n" % sentence)


def read_pos_tags_to_file(file):
    with open(str(file), 'r') as f:
        lines = f.readlines()
        for line in lines:
            for chunk in line.chunks:
                print(chunk.type, [(w.string, w.type) for w in chunk.words])


def parser(tagger_name, dataframe, output_filepath):
    print("parsing using " + tagger_name)
    if tagger_name == "alpino":
        pos_tag = train_dutch_tagger()
        parsed_text = pos_tag_text_alpino(dataframe['Brontekst'], pos_tag)
        dataframe['tags'] = parsed_text
        write_df_to_csv(dataframe, output_filepath)
        fle = "postags_alpino.txt"
    elif tagger_name == "pattern":
        parsed_text, parsed_verbs = parse_text_pattern(dataframe['Brontekst'])
        fle = "postags_pattern-nl.txt"
        dataframe['verbs'] = parsed_verbs
        dataframe['tags'] = parsed_text
        write_df_to_csv(dataframe, output_filepath)
    elif tagger_name == "spacy":
        parsed_text = pos_dep_tag_text_spacy(dataframe['Brontekst'])
        dataframe['tags'] = parsed_text
        write_df_to_csv(dataframe, output_filepath)
    else:
        print("parser not recognized")

    return dataframe


if __name__ == '__main__':
    cwd = os.getcwd()
    selected_parser = "pattern"
    input_dataframe = pd.read_csv(os.path.join(cwd, "src", "resource", "data", "csv_files",
                                               "BWBR0011823_2019-02-27_Vreemdelingenwet.csv"), sep=";")
    output_file_name = f"BWBR0011823_2019-02-27_Vreemdelingenwet_{selected_parser}.csv"
    output_df_path = os.path.join(cwd, "src", "resource", "data", "csv_files", "postagged", output_file_name)
    parser(selected_parser, input_dataframe, output_df_path)
