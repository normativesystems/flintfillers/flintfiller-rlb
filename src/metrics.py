"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import string
import pandas as pd
import nltk
from nltk import sent_tokenize


def count_metrics_law(csv_location, column) -> int:
    data = pd.read_csv(csv_location)
    nwords = data[column].str.split().map(len).sum()
    nlines = data[column].count()
    nsentences = 0
    for line in data[column]:
        nsentences = nsentences + 1 + len(sent_tokenize(line))
    nunique_words = len(set([x for i in data[column].str.split().apply(
        lambda x: [''.join([y for y in i if y not in string.punctuation]) for i in x]).tolist() for x in i]))
    return nsentences, nwords, nunique_words, nlines


print(count_metrics_law('C:/Users/bakkerrm/git/flintfiller/data/csv_files/BWBR0011823_2019-02-27_Vreemdelingenwet.csv',
                        'Brontekst'))
print(count_metrics_law(
    'C:/Users/bakkerrm/git/flintfiller/data/csv_files/BWBR0005537_2019-11-14_Algemene_wet_bestuursrecht.csv',
    'Brontekst'))
