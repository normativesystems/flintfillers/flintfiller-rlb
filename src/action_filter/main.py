"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import argparse
import pandas as pd
import os

from extract_action_sentences import prepare_2022_sentences
from reuse_previous_annotations import prepare_2021_sentences
from filter_action_sentences import filter_action_sentences
from split_annotators import assign_sentences_uneven
from split_annotators import assign_sentences_to_annotators
cwd = os.getcwd()


def main(action_verb_path: str):
    args = parse_commandline_arguments()
    final_2021_df = prepare_2021_sentences()
    final_2022_df = prepare_2022_sentences()
    merged = pd.concat([final_2021_df, final_2022_df])
    filtered = filter_action_sentences(merged, action_verb_path)
    # assign sentences to annotators
    output_path = args.final_output_path
    candidates_dict = {"candidate1": 200, "candidate2": 200, "candidate3": 400, "candidate4": 400, "candidate5": 400,
                       "candidate6": 400, "candidate7": 200, "candidate8": 400}
    required_annotations = assign_sentences_uneven(candidates_dict)
    filtered_assigned_annotators = assign_sentences_to_annotators(required_annotations, filtered)
    filtered_assigned_annotators.to_csv(output_path, sep=";", index=False)

    return print(f"output file stored in: {output_path}")


# python src/action_filter/main.py -lf src/resource/data/annotations/all_input_laws_as_xml -d src/resource/data/annotations/annotations_2022/placeholder.json -df src/resource/data/annotations/annotations_2022/annotation_sentences_2022.csv -fo src/resource/data/annotations/annotations_2022/final_output.csv


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--xml',
                        help="input xml file")
    parser.add_argument('-d', '--dict_file',
                        help="location of file with dictionary of xml")
    parser.add_argument('-df', '--df_file',
                        help="outputfile of 2022 annotations")
    parser.add_argument('-fo', '--final_output_path',
                        help="output path of the complete file that is filtered and has annotators assigned")
    parser.add_argument('-lf', '--law_folder',
                        help="location of law folder containing laws as xml or json.")

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main(os.path.join("src", "resource", "data", "annotations", "annotations_2022", "action_sentences.xlsx"))

