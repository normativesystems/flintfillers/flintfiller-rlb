"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import pandas as pd
import os
import operator

from ast import literal_eval
from collections import defaultdict, OrderedDict
from pattern.text.nl import parse, split, conjugate

from flintfiller.preprocess_pattern_spacy_output import reformat_pattern_chunk
from flintfiller.preprocess_pattern_spacy_output import drop_non_matching_tokens
from flintfiller.preprocess_pattern_spacy_output import combine_pattern_spacy


def get_object(dependency: str) -> str:
    if dependency == 'obj' or dependency == 'nsubj:pass':
        return True


def get_actor(dependency: str) -> str:
    if dependency == 'nsubj' or dependency == 'obl:agent':
        return True


# def get_action(dependency: str, input_token: str) -> str:
#     if (dependency == 'ROOT' or dependency == 'ccomp' or dependency == 'xcomp') and input_token in list_of_action_verbs:
#         return True


def get_inf(token: str):
    """
    Retrieve the infinitve and original of a verb.
    :param token:
    :return:
    """
    inf = ""
    original = ""
    verbs_per_token = {}
    s = parse(token)
    pos_tagged_token = split(s)[0]
    # check if list of chunks is not empty
    if pos_tagged_token.chunks:
        chunks = pos_tagged_token.chunks[0]
        words = chunks.words[0]
        if 'VB' in words.type:
            verbs_per_token[conjugate(str(words), 'INFINITIVE')] = str(words)
            for k, v in verbs_per_token.items():
                inf = k
                original = v
    return inf, original


def check_infinitive(inf, list_of_action_verbs) -> bool:
    return inf in list_of_action_verbs


def get_action(dependency: str, input_token: str, list_of_action_verbs: list) -> str:
    if dependency == 'ROOT' or dependency == 'ccomp' or dependency == 'xcomp':
        inf, original = get_inf(input_token)
        if check_infinitive(inf, list_of_action_verbs) and not original == 'indien':
            return True


def read_action_words(filepath):
    """
    Reads and selects the action verbs that are labelled as action by selecting only the rows that have "ja" in the
    "action" column.
    :param filepath: location of the excel sheet.
    :return: list of the base of the action verbs.
    """
    action_words_excel = pd.read_excel(filepath, sheet_name="action_sentences_robert_oov3", engine='openpyxl')
    action_words_excel = action_words_excel[action_words_excel["action"] == "ja"]
    action_words = list(action_words_excel['base'])
    return action_words


def add_roles(pattern_list: list, spacy_list: list, list_of_action_verbs: list) -> dict:
    """
    Add the flint roles based on the dependencies and get_action(), get_actor() and get_object() functions defined
    above.
    :param pattern_list:
    :param spacy_list:
    :param list_of_action_verbs:
    :return: a dictionary with the chunks as key and lists structured as [token, pos, dep, role], if no role is
    available and empty string is returned.
    """
    store_result = defaultdict(list)

    for pattern_lst, spacy_lst in zip(pattern_list, spacy_list):
        token = spacy_lst[0]
        pos = spacy_lst[1]
        dep = spacy_lst[2]
        chunk = pattern_lst[2]
        # check role
        if get_object(dep):
            role = "Object"
        elif get_action(dep, token, list_of_action_verbs):
            role = "Action"
        elif get_actor(dep):
            role = "Actor"
        else:
            role = ""
        store_result[chunk].append([token, pos, dep, role])
    return store_result


equal_freq_role = []


def find_most_frequent_role_chunk(output_dict):
    """
    Loop through the dictionary with chunks as keys and check if it contains a role, if a role is found assign this
    role to the entire chunk. If multiple roles are found the role with the highest frequency is selected. If two role
    have a equal frequencies TO DO
    :param output_dict: a dictionary with the chunks as key and lists structured as [token, pos, dep, role], if no role
    is available and empty string is returned.
    :return: A dictionary that contains the most frequent role for each chunk, i.e. dict[VP0] = 'action', dict[NP3] =
    'object'
    """

    chunk_role_dict = {}

    for chunk, lst in output_dict.items():
        temp_freq_dict = {}
        for list_of_objects in lst:
            if list_of_objects[3] != '':
                if list_of_objects[3] not in temp_freq_dict:
                    temp_freq_dict[list_of_objects[3]] = 1
                else:
                    temp_freq_dict[list_of_objects[3]] += 1

        temp_freq_dict = sorted(temp_freq_dict.items(), reverse=True, key=operator.itemgetter(1))
        temp_freq_dict = OrderedDict(temp_freq_dict)

        # wat te doen als er twee rollen evenveel voorkomen in één chunk?
        # TODO : komt dit uberhaupt voor?
        if len(temp_freq_dict) > 1:
            most_frequent_role_frequency = list(temp_freq_dict.values())[0]
            second_most_frequent_role_frequency = list(temp_freq_dict.values())[1]
            equal_freq_role.append(temp_freq_dict)

        # check if dictionary is empty
        if not temp_freq_dict:
            most_frequent_role = ""
        else:
            most_frequent_role = list(temp_freq_dict)[0]
        chunk_role_dict[chunk] = most_frequent_role
    return chunk_role_dict


def assign_labels_to_chunk(dct: dict, output_dict: dict):
    """
    Loop through a dictionary with chunks as keys and list of lists structured as [token, pos, dep] as values.
    Use the other dictionary as key mapping to find the most frequent role, output all the tokens that belong to this
    role.
    :param dct: a mapping dictionary that contains the most frequent role for each chunk, i.e. [NP3] = 'object'
    :param output_dict: a dictionary with the chunks as key and lists structured as [token, pos, dep, role], if no role
     is available and empty string is returned.
    :return: a dictionary with 'object', 'action' and 'actor' as keys and the tokens that belong to this role as strings
    """
    dict_with_roles = defaultdict(str)
    for chunk_id, sub_lst in output_dict.items():
        for list_of_objects in sub_lst:
            most_frequent_role = dct[chunk_id]
            # if most frequent role is not empty
            if most_frequent_role:
                dict_with_roles[most_frequent_role] += f"{str(list_of_objects[0])} "
    return dict_with_roles


def roles_to_list_for_evaluation(dct: dict):
    """
    Transform the dictionary with roles to a list of lists so that the output can be easily compared.
    :param dct: a dictionary with the chunks as key and lists structured as [token, pos, dep, role], if no role is
    available and empty string is returned
    :return:
    """
    punctuation_tokens = [",", ".", ":", "(", ")", ";"]
    sentence_lst_token = []
    sentence_lst_role = []
    sentence_token_role_tuple = []
    sentence_lst_all_features = []
    for chunk_tag, list_of_lists in dct.items():
        most_frequent_role = [x[3] for x in list_of_lists if not x[3] == '']
        # most_frequent_role is a single list if there is a role, otherwise its empty which means there is no role
        if most_frequent_role:
            most_frequent_role = most_frequent_role[0]
        else:
            most_frequent_role = "O"
        for token, pos, dep, role in list_of_lists:
            if token in punctuation_tokens:
                most_frequent_role = token
            sentence_lst_token.append(token)
            sentence_lst_role.append(most_frequent_role)
            sentence_token_role_tuple.append((token, most_frequent_role))
            sentence_lst_all_features.append((token, pos, dep, chunk_tag, most_frequent_role))
        most_frequent_role = ""
    return sentence_lst_token, sentence_lst_role, sentence_token_role_tuple, sentence_lst_all_features


def add_phrase_rule(original_sentence, pattern_sentence, spacy_sentence, list_of_action_verbs,
                    output_evaluation_file: bool = False):
    reformatted_pattern_chunk = reformat_pattern_chunk(pattern_sentence)
    equal_tokens_list = drop_non_matching_tokens(spacy_sentence, reformatted_pattern_chunk)
    equal_length_pattern = combine_pattern_spacy(equal_tokens_list, spacy_sentence)
    chunk_with_roles_dict = add_roles(equal_length_pattern, spacy_sentence, list_of_action_verbs)

    # write the output to a file for evaluation
    if output_evaluation_file:
        sentence_lst_token, sentence_lst_role, sentence_token_role_tuple, sentence_lst_all_features = \
            roles_to_list_for_evaluation(chunk_with_roles_dict)
        return original_sentence, sentence_lst_token, sentence_lst_role, \
               sentence_token_role_tuple, sentence_lst_all_features
    else:
        frequent_role_mapping = find_most_frequent_role_chunk(chunk_with_roles_dict)
        dict_with_roles = assign_labels_to_chunk(frequent_role_mapping, chunk_with_roles_dict)
        return dict_with_roles


def main(pattern_path, spacy_path, list_of_action_verbs, output_filename, output_evaluation_file: bool = False):
    """
    Read in a pos- and chunk tagged file using pattern and a pos- and dependency tagged file by spacy. Both files are
    preprocessed to ensure that the input token are the same (see preprocess_pattern_spacy_output.py for details).
    Based on chunk and dependency tags the following semantic role label tags are assigned:
    ["Object", "Actor", "Action"] (see def add_roles for details on the rules).

    :param pattern_path: path to dataframe that is chunk- and postagged using pattern
    :param spacy_path: path to dataframe that is dependency- and postagged using spacy
    :param list_of_action_verbs: list of action verbs
    :param output_filename: name of the output file
    :param output_evaluation_file: if True, a file with the SRL tags are written to disk.
    :return:
    """
    pattern_df = pd.read_csv(pattern_path, sep=";")
    spacy_df = pd.read_csv(spacy_path, sep=";")

    # store output
    sentence_ids = []
    original_sentences = []
    labeled_sentences_token = []
    labeled_sentences_role = []
    labeled_sentences_tuple = []
    labeled_sentences_additional_features = []

    if 'sentence_id' not in pattern_df.columns:
        pattern_df['sentence_id'] = "placeholder_sent_id"
    for sent_id, original_sent, pattern_sent, spacy_sent in zip(list(pattern_df['sentence_id']),
                                                                list(pattern_df['Brontekst']), pattern_df['tags'],
                                                                spacy_df['tags']):
        pattern_sent = literal_eval(pattern_sent)
        spacy_sent = literal_eval(spacy_sent)
        if output_evaluation_file:
            # original_sentence, sentence_lst_token, sentence_lst_role, sentence_token_role_tuple, sentence_lst_all_features
            original_sentence, tokens, roles, labeled_sentence, labeled_sentence_additional_features = \
                add_phrase_rule(original_sent, pattern_sent, spacy_sent, list_of_action_verbs, output_evaluation_file)

            sentence_ids.append(sent_id)
            original_sentences.append(original_sentence)
            labeled_sentences_token.append(tokens)
            labeled_sentences_role.append(roles)
            labeled_sentences_tuple.append(labeled_sentence)
            labeled_sentences_additional_features.append(labeled_sentence_additional_features)


        else:
            dct = add_phrase_rule(original_sent, pattern_sent, spacy_sent, list_of_action_verbs, False)

    if output_evaluation_file:
        # merge in one dataframe
        merged = pd.DataFrame(
            {'sentence_id': sentence_ids,
             'Brontekst': original_sentences,
             'tokens': labeled_sentences_token,
             'srl_tags': labeled_sentences_role,
             'srl_tags_rulebased': labeled_sentences_tuple,
             'labeled_sentences_additional_features': labeled_sentences_additional_features
             })
        output_file_path = os.path.join(cwd, "src", "resource", "data", "csv_files",
                                        f"{output_filename}.csv")
        merged.to_csv(output_file_path, index=False, sep=";")

        return print(f"output file created in: {output_file_path}")


if __name__ == '__main__':
    cwd = os.getcwd()

    list_of_action_verbs = read_action_words(os.path.join(cwd, "src", "resource", "data",
                                                          "annotations", "annotations_2022",
                                                          "action_sentences.xlsx"))

    main(os.path.join(cwd, "src", "resource", "data", "csv_files", "postagged",
                      "BWBR0011823_2019-02-27_Vreemdelingenwet_pattern.csv"),
         os.path.join(cwd, "src", "resource", "data", "csv_files", "postagged",
                      "BWBR0011823_2019-02-27_Vreemdelingenwet_spacy.csv"), list_of_action_verbs,
         "BWBR0011823_2019-02-27_Vreemdelingenwet_pattern_spacy",
         True)
