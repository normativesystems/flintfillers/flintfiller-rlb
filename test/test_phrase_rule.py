import os
import pandas as pd
from ast import literal_eval

from src.flintfiller.preprocess_pattern_spacy_output import reformat_pattern_chunk
from src.flintfiller.preprocess_pattern_spacy_output import drop_non_matching_tokens
from src.flintfiller.preprocess_pattern_spacy_output import combine_pattern_spacy
from src.flintfiller.assign_semantic_roles_pattern_spacy_input import get_inf


def test_reformat_pattern_chunk():
    test_sent = "[['VP', ('In', 'VB')], ['NP', ('deze', 'PRP'), ('wet', 'NN')], ['VP', ('daarop', 'RB'), " \
                "('berustende', 'VBG')], ['NP', ('bepalingen', 'NNS')], ['VP', ('wordt', 'MD'), ('verstaan', 'VB'), " \
                "('onder', 'RP')]]"

    test_sent2 = "[['NP', ('vreemdeling', 'NN')], ['NP', ('ieder', 'PRP'), ('die', 'PRP'), " \
                 "('de', 'DT'), ('Nederlandse', 'JJ'), ('nationaliteit', 'NN')], ['ADVP', ('niet', 'RB')], " \
                 "['NP', ('bezit', 'NN')], ['ADVP', ('niet', 'RB')], ['PP', ('op', 'IN')], ['NP', ('grond', 'NN')], " \
                 "['PP', ('van', 'IN')], ['NP', ('een', 'DT'), ('wettelijke', 'JJ'), ('bepaling', 'NN'), " \
                 "('als', 'PRP'), ('Nederlander', 'NN')], ['VP', ('moet', 'MD'), ('worden', 'MD'), " \
                 "('behandeld', 'VBN')]]"

    test_sent3 = "[['NP', ('buitengrenzen', 'NNS')], ['NP', ('de', 'DT'), ('Nederlandse', 'JJ'), " \
                 "('zeegrenzen', 'NNS')], ['NP', ('lucht', 'NN')], ['NP', ('zeehavens', 'NNS')], ['ADVP', " \
                 "('waar', 'RB')], ['NP', ('grenscontrole', 'NN')], ['PP', ('op', 'IN')], " \
                 "['NP', ('personen', 'NNS')], ['VP', ('wordt', 'MD'), ('uitgeoefend', 'VBN')]]"

    output1 = reformat_pattern_chunk(literal_eval(test_sent))
    output2 = reformat_pattern_chunk(literal_eval(test_sent2))
    output3 = reformat_pattern_chunk(literal_eval(test_sent3))

    assert len(output1) == 9
    assert len(output2) == 20
    assert len(output3) == 12

    # check random instances of each output
    assert output1[0] == ('In', 'VB', 'VP0')
    assert output2[5] == ('nationaliteit', 'NN', 'NP1')
    assert output3[8] == ('op', 'IN', 'PP6')


def test_drop_non_matching_tokens():
    test1 = literal_eval(
        "('richtlijn', 'PLACEHOLDER', 'PLACEHOLDER'), ('nr.', 'PLACEHOLDER', 'PLACEHOLDER'), ('minister', 'PLACEHOLDER', 'PLACEHOLDER'), ('van', 'PLACEHOLDER', 'PLACEHOLDER'), ('het', 'PLACEHOLDER', 'PLACEHOLDER')")
    test2 = literal_eval(
        "('richtlin', 'PLACEHOLDER', 'PLACEHOLDER'), ('nr', 'PLACEHOLDER', 'PLACEHOLDER'), ('minister', 'PLACEHOLDER', 'PLACEHOLDER'), ('van', 'PLACEHOLDER', 'PLACEHOLDER'), ('het', 'PLACEHOLDER', 'PLACEHOLDER')")

    lst = drop_non_matching_tokens(test1, test2)
    # only select the tokens for testing purposes
    all_tokens = [x[0] for x in lst]
    assert not "richtlijn" in all_tokens, "'richtlijn' is spelled differently in test1 and test2 so should not be in " \
                                          "the output list"
    assert not "nr." in all_tokens, "'nr' is spelled differently in test1 and test2 so should not be in " \
                                    "the output list"
    assert "minister" in all_tokens
    assert "van" in all_tokens
    assert "het" in all_tokens


def test_combine_pattern_spacy():
    pattern_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet_pattern.csv", sep=";")
    spacy_df = pd.read_csv("test_data/BWBR0011823_2019-02-27_Vreemdelingenwet_spacy.csv", sep=";")

    # Sentence with a difference in token: position 5 spacy = 'nr.', position 5 pattenr = 'nr' -> the difference in the
    # stop sign. Removing '.' from the end of tokens does NOT help in succeeding this test, there is another problem
    spacy1 = literal_eval(spacy_df['tags'][27])
    pattern1 = literal_eval(pattern_df['tags'][27])
    pattern1 = reformat_pattern_chunk(pattern1)
    pattern1 = drop_non_matching_tokens(spacy1, pattern1)
    combined_output1 = combine_pattern_spacy(pattern1, spacy1)
    assert [x[0] for x in spacy1] == [y[0] for y in combined_output1], "tokens do not match"
    all_chunks = [x[2] for x in combined_output1]
    only_placeholders = True
    for x in all_chunks:
        if "PLACEHOLDER" not in x:
            only_placeholders = False
    assert not only_placeholders, "The combined output only contained placeholder chunks which are inserted when " \
                                  "spacy and pattern tokens do not match which should not be the case"



    # "Simple" sentence that is relatively easy to parse
    spacy2 = literal_eval(spacy_df['tags'][4])
    pattern2 = literal_eval(pattern_df['tags'][4])
    pattern2 = reformat_pattern_chunk(pattern2)
    pattern2 = drop_non_matching_tokens(spacy2, pattern2)
    combined_output2 = combine_pattern_spacy(pattern2, spacy2)
    assert [x[0] for x in spacy2] == [y[0] for y in combined_output2], "Should be true"
    all_chunks = [x[2] for x in combined_output2]
    only_placeholders = True
    for x in all_chunks:
        if "PLACEHOLDER" not in x:
            only_placeholders = False
    assert not only_placeholders, "The combined output only contained placeholder chunks which are inserted when " \
                                  "spacy and pattern tokens do not match which should not be the case"

    # Sentence with a difference in token: position 8 spacy = 'lucht-', position 8 pattern = 'lucht' -> the difference
    # is in the dash. Removing '-' from the end of tokens helps for this sentence (using this as a preprocessing step
    # will make this test succeed, otherwise it will fail.
    spacy3 = literal_eval(spacy_df['tags'][6])
    pattern3 = literal_eval(pattern_df['tags'][6])
    pattern3 = reformat_pattern_chunk(pattern3)
    pattern3 = drop_non_matching_tokens(spacy3, pattern3)
    combined_output3 = combine_pattern_spacy(pattern3, spacy3)
    assert [x[0] for x in spacy3] == [y[0] for y in combined_output3], "Should be true"
    all_chunks = [x[2] for x in combined_output3]
    only_placeholders = True
    for x in all_chunks:
        if "PLACEHOLDER" not in x:
            only_placeholders = False
    assert not only_placeholders, "The combined output only contained placeholder chunks which are inserted when " \
                                  "spacy and pattern tokens do not match which should not be the case"


def test_get_inf():
    test_token1 = "uitgeoefend"
    inf1, original1 = get_inf(test_token1)
    assert inf1 == "uitoefenen"
    assert original1 == "uitgeoefend"

    test_token2 = "regelt"
    inf2, original2 = get_inf(test_token2)
    assert inf2 == "regelen"
    assert original2 == "regelt"

    test_token3 = "benoemt"
    inf3, original3 = get_inf(test_token3)
    assert inf3 == "benoemen"
    assert original3 == "benoemt"

    # if the token is not a verb then the output should be an empty string
    test_token4 = "appel"
    inf4, original4 = get_inf(test_token4)
    assert inf4 == ""
    assert original4 == ""
