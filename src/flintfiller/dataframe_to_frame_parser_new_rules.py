"""
    Copyright (C) 2020 Nederlandse Organisatie voor Toegepast Natuur-
    wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for
    applied scientific research


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    @author: Roos Bakker, Romy van Drie, Daan Vos, Maaike de Boer
    @contact: roos.bakker@tno.nl, maaike.deboer@tno.nl
"""

import json
import pandas as pd
import ast
import os

from flintfiller.assign_semantic_roles_pattern_spacy_input import add_phrase_rule
from flintfiller.assign_semantic_roles_pattern_spacy_input import read_action_words

action_verbs = ['aanbrengen', 'aanwijzen', 'achterwege blijven', 'afnemen', 'afwijken', 'afwijzen',
                'ambtshalve verlenen', 'ambtshalve verlengen', 'annuleren', 'behandelen', 'beheren', 'bepalen',
                'beperken', 'betreden', 'beveiligen', 'bevelen', 'bevorderen', 'bieden gelegenheid', 'bijhouden',
                'buiten behandeling stellen', 'buiten werking stellen', 'doorzoeken', 'erop wijzen',
                'gebruiken maken van', 'gedwongen ontruimen', 'geven', 'heffen', 'in bewaring stellen',
                'in de gelegenheid stellen zich te doen horen', 'in kennis stellen', 'in werking doen treden',
                'in werking stellen', 'indienen', 'innemen', 'instellen', 'intrekken', 'invorderen', 'inwilligen',
                'maken', 'naar voren brengen', 'nemen', 'niet in behandeling nemen', 'niet-ontvankelijk verklaren',
                'nogmaals verlengen', 'om niet vervoeren', 'onderwerpen', 'onderzoeken', 'ongewenstverklaren',
                'onmiddellijk bepalen', 'onmiddellijk verlaten', 'ontnemen', 'ontvangen', 'opheffen', 'opleggen',
                'oproepen', 'overbrengen', 'overdragen', 'plaatsen', 'schorsen', 'schriftelijk in kennis stellen',
                'schriftelijk laten weten', 'schriftelijk mededelen', 'schriftelijk naar voren brengen', 'signaleren',
                'sluiten', 'staande houden', 'stellen', 'straffen', 'ter hand stellen', 'teruggeven',
                'tijdelijk in bewaring nemen', 'toetsen', 'toezenden', 'uitstellen', 'uitvaardigen', 'uitzetten',
                'van rechtswege verkrijgen', 'vaststellen', 'vergelijken', 'verhalen', 'verhogen', 'verklaren',
                'verkorten', 'verkrijgen', 'verlaten', 'verlenen', 'verlengen', 'verplichten', 'verschaffen',
                'verstrekken', 'verzoeken', 'voegen', 'vorderen', 'vragen', 'willigen', 'weigeren', 'wijzigen']

list_act = []


def read_csv_to_df(csv_file):
    datafrm = pd.read_csv(csv_file, sep=";")
    print("csv loaded from " + csv_file)
    return datafrm


def write_df_to_csv(df, fle):
    df.to_csv(fle, sep=";")
    print("df written to " + fle)


def get_empty_flint_frame_format() -> dict:
    flint_frame = {
        "acts": [],
        "facts": [],
        "duties": []
    }
    return flint_frame


def get_empty_act_frame() -> dict:
    act_frame = {
        "act": "",
        "actor": "",
        "action": "",
        "object": "",
        "recipient": "",
        "preconditions": {
            "expression": "LITERAL",
            "operand": True
        },
        "create": [],
        "terminate": [],
        "sources": [],  # with validFrom, validTo, citation juriconnect and text
        "explanation": ""
    }
    return act_frame


def check_infinitive(inf, row) -> bool:
    return inf in action_verbs and not \
        ("het " + inf) in row['Brontekst'] or \
           ("de " + inf) in row['Brontekst'] or \
           ("een " + inf) in row['Brontekst']


def write_flint_frames_to_json(flint_frames, flint_file):
    with open(str(flint_file), 'w') as f:
        json.dump(flint_frames, f)
    print("flint frames written to " + str(flint_file))


def get_action_frame_phrase_rule(original_sentence: list, dep_tagged_sentence: list,
                                 chunk_tagged_sentence: list, list_of_action_verbs: list) -> dict:
    frame = get_empty_act_frame()

    dict_with_roles = add_phrase_rule(original_sentence, chunk_tagged_sentence, dep_tagged_sentence,
                                      list_of_action_verbs)
    if dict_with_roles['Action']:
        action = dict_with_roles['Action'].rstrip()
        frame['action'] = f"[{action}]"
    else:
        action = ""

    if dict_with_roles['Object']:
        object_role = dict_with_roles['Object'].rstrip()
        frame['object'] = f"[{object_role}]"
    else:
        object_role = ""

    if dict_with_roles['Actor']:
        frame['actor'] = f"[{dict_with_roles['Actor'].rstrip()}]"
    frame['act'] = f"<<{action} {object_role}>>"
    return frame


def create_frames_phrase_rule(dep_tagged_df, chunk_tagged_df, list_of_action_verbs, output_file):
    flint_frame_format = get_empty_flint_frame_format()
    flint_frames = []

    for original_sents, dep, chunk in zip(dep_tagged_df[['Brontekst']].items(),
                                          dep_tagged_df[['tags']].items(),
                                          chunk_tagged_df[['tags']].items()):
        for original_sent, dep_row, chunk_row in zip(original_sents[1], dep[1], chunk[1]):
            flint_frames.append(get_action_frame_phrase_rule(original_sent,
                                                             ast.literal_eval(dep_row),
                                                             ast.literal_eval(chunk_row), list_of_action_verbs))

    flint_frame_format['acts'] = flint_frames
    write_flint_frames_to_json(flint_frame_format, output_file)
    return flint_frame_format


if __name__ == '__main__':
    cwd = os.getcwd()
    pos_dep_tagged_csv = os.path.join(cwd, "src", "resource", "data", "csv_files", "postagged",
                                      "BWBR0011823_2019-02-27_Vreemdelingenwet_spacy.csv")
    pos_chunk_tagged_csv = os.path.join(cwd, "src", "resource", "data", "csv_files", "postagged",
                                        "BWBR0011823_2019-02-27_Vreemdelingenwet_pattern.csv")
    pos_dep_tagged_df = read_csv_to_df(str(pos_dep_tagged_csv))
    pos_chunk_tagged_df = read_csv_to_df(str(pos_chunk_tagged_csv))

    cwd = os.getcwd()
    list_of_action_verbs = read_action_words(os.path.join(cwd, "src", "resource", "data",
                                                          "annotations", "annotations_2022",
                                                          "action_sentences.xlsx"))

    # tussenweg versie met dependencies
    # create_frames(
    #     pos_dep_tagged_df,
    #     output_file=os.path.join(cwd, "src", "resource", "data", "flint_frames", "test_frame_spacy.json"))

    # phrase rule
    create_frames_phrase_rule(pos_dep_tagged_df, pos_chunk_tagged_df, list_of_action_verbs,
                              output_file=os.path.join(cwd, "src", "resource", "data", "flint_frames",
                                                       "flint_frames_BWBR0011823_2019-02-27_Vreemdelingenwet_pattern_spacy.json"))
